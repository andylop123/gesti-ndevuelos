create database gestion_vuelos



INSERT INTO usuarios(cedula, nombre, edad, contrasena, tipo) VALUES
('12345678','Andrey',21,'123','Administrador')


create table usuarios(
	id serial primary key,
	id_aerolinea integer,
	cedula text not null,
	nombre text not null,
	edad integer,
	contrasena text,
	tipo text not null,
	fecha date,
	rol text,
	estado text
)

INSERT INTO usuarios(cedula, nombre, edad, contrasena, tipo) VALUES
('12345','Andrey',20,'827CCB0EEA8A706C4C34A16891F84E7B','Administrador')


INSERT INTO usuarios(cedula, nombre, edad, contrasena, tipo) VALUES
('123','Andrey',20,'202CB962AC59075B964B07152D234B70','Pasajero')
select * from usuarios





create table aerolineas(
	id serial primary key,
	nombre text not null,
	ano text not null,
	tipo text not null
)

insert into aerolineas(nombre,ano,tipo)values('Volaris','2016','Regional');

insert into aerolineas(nombre,ano,tipo)values('Delta','2015','Gran escala');

insert into aerolineas(nombre,ano,tipo)values('Latam','2017','Regional');

insert into aerolineas(nombre,ano,tipo)values('American','2020','Gran escala');

insert into aerolineas(nombre,ano,tipo)values('Skyway','2002','Local');
select * from aerolineas 




create table tripulaciones(
	id serial primary key,
	id_aerolinea integer not null,
	cedula text not null,
	nombre text not null,
	fecha date not null,
	rol text not null,
	estado text not null
)

--Primeros 5
insert into tripulaciones(id_aerolinea,cedula,nombre,fecha,rol,estado)
values(1,'104780268','Luis','23-08-1997','Piloto','Disponible');

insert into tripulaciones(id_aerolinea,cedula,nombre,fecha,rol,estado)
values(1,'301580863','Maria','21-02-1998','Piloto','Disponible');

insert into tripulaciones(id_aerolinea,cedula,nombre,fecha,rol,estado)
values(1,'203180251','Valeria','23-01-1997','Servicio al cliente','Disponible');

insert into tripulaciones(id_aerolinea,cedula,nombre,fecha,rol,estado)
values(1,'203890118','Carlos','15-05-1999','Servicio al cliente','Disponible');

insert into tripulaciones(id_aerolinea,cedula,nombre,fecha,rol,estado)
values(1,'501390128','Juan','11-05-1999','Servicio al cliente','Disponible');


--Segunda tripulacion
insert into tripulaciones(id_aerolinea,cedula,nombre,fecha,rol,estado)
values(1,'106780268','Beto','23-08-1997','Piloto','Disponible');

insert into tripulaciones(id_aerolinea,cedula,nombre,fecha,rol,estado)
values(1,'701580863','Josa','21-02-1998','Piloto','Disponible');

insert into tripulaciones(id_aerolinea,cedula,nombre,fecha,rol,estado)
values(1,'906180251','Juanca','23-01-1997','Servicio al cliente','Disponible');

insert into tripulaciones(id_aerolinea,cedula,nombre,fecha,rol,estado)
values(1,'303590118','Juanes','15-05-1999','Servicio al cliente','Disponible');

insert into tripulaciones(id_aerolinea,cedula,nombre,fecha,rol,estado)
values(1,'301390728','Bertulia','11-05-1999','Servicio al cliente','Disponible');


--Tercer tripulacion
insert into tripulaciones(id_aerolinea,cedula,nombre,fecha,rol,estado)
values(1,'104750268','Bryan','23-08-1997','Piloto','Disponible');

insert into tripulaciones(id_aerolinea,cedula,nombre,fecha,rol,estado)
values(1,'304380863','Jeffry','21-02-1998','Piloto','Disponible');

insert into tripulaciones(id_aerolinea,cedula,nombre,fecha,rol,estado)
values(1,'102280251','Beatriz','23-01-1997','Servicio al cliente','Disponible');

insert into tripulaciones(id_aerolinea,cedula,nombre,fecha,rol,estado)
values(1,'204440118','Joselyn','15-05-1999','Servicio al cliente','Disponible');

insert into tripulaciones(id_aerolinea,cedula,nombre,fecha,rol,estado)
values(1,'801890128','Juana','11-05-1999','Servicio al cliente','Disponible');

--Cuata Tripulacion
insert into tripulaciones(id_aerolinea,cedula,nombre,fecha,rol,estado)
values(1,'104780255','Lusiana','23-08-1997','Piloto','Disponible');

insert into tripulaciones(id_aerolinea,cedula,nombre,fecha,rol,estado)
values(1,'301580866','Maria Jose','21-02-1998','Piloto','Disponible');

insert into tripulaciones(id_aerolinea,cedula,nombre,fecha,rol,estado)
values(1,'203180258','Juan','23-01-1997','Servicio al cliente','Disponible');

insert into tripulaciones(id_aerolinea,cedula,nombre,fecha,rol,estado)
values(1,'203890998','Benito','15-05-1999','Servicio al cliente','Disponible');

insert into tripulaciones(id_aerolinea,cedula,nombre,fecha,rol,estado)
values(1,'501390118','Erick','11-05-1999','Servicio al cliente','Disponible');

--Quinta Tripulacion
insert into tripulaciones(id_aerolinea,cedula,nombre,fecha,rol,estado)
values(1,'100780268','Patricia','23-08-1997','Piloto','Disponible');

insert into tripulaciones(id_aerolinea,cedula,nombre,fecha,rol,estado)
values(1,'301000863','Walter','21-02-1998','Piloto','Disponible');

insert into tripulaciones(id_aerolinea,cedula,nombre,fecha,rol,estado)
values(1,'203680251','Marcos','23-01-1997','Servicio al cliente','Disponible');

insert into tripulaciones(id_aerolinea,cedula,nombre,fecha,rol,estado)
values(1,'203890855','Hernan','15-05-1999','Servicio al cliente','Disponible');

insert into tripulaciones(id_aerolinea,cedula,nombre,fecha,rol,estado)
values(1,'501390444','Greivin','11-05-1999','Servicio al cliente','Disponible');


select * from tripulaciones


create table aviones(
	id serial primary key,
	id_aerolinea integer not null,
	modelo text not null,
	ano date not null,
	capacidad text not null,
	estado text not null
)


insert into aviones(id_aerolinea,modelo,ano,capacidad,estado)
values(1,'Boeing 747','2018','712','Disponible');

insert into aviones(id_aerolinea,modelo,ano,capacidad,estado)
values(1,'Airbus A320','2016','812','Disponible');

insert into aviones(id_aerolinea,modelo,ano,capacidad,estado)
values(1,'Túpolev Tu-204','2020','792','Disponible');

insert into aviones(id_aerolinea,modelo,ano,capacidad,estado)
values(1,'Into 947','2018','712','Disponible');

insert into aviones(id_aerolinea,modelo,ano,capacidad,estado)
values(1,'Airbus A380','2020','842','Disponible');

select * from aviones



create table aeropuertos(
	id serial primary key,
	iata text not null,
	nombre text not null,
	pais text not null
)


insert into aeropuertos(iata,nombre,pais)
values('SJO','Aeropuerto Internacional de San José/Juan Santamaría','Costa Rica');

insert into aeropuertos(iata,nombre,pais)
values('MEX','Aeropuerto Internacional de la Ciudad de México','Mexico');

insert into aeropuertos(iata,nombre,pais)
values('"ARVM"','Aeropuerto Internacional Ramón Villeda Morales','Honduras');

insert into aeropuertos(iata,nombre,pais)
values('JFK','Aeropuerto Internacional John F. Kennedy','Estados Unidos');

insert into aeropuertos(iata,nombre,pais)
values('MAD','Aeropuerto de Madrid-Barajas Adolfo Suárez','España');

select * from aeropuertos

create table vuelos(
	id serial primary key,
	id_avion integer not null,
	id_aerolinea integer not null,
	id_aeropuerto_salida integer not null,
	id_aeropuerto_llegada integer not null,	
	precio integer not null,
	fecha_salida timestamp not null,
	hora_salida time not null,
	hora_llegada time not null,
	duracion integer not null,
	fecha_llegada timestamp not null,
	tripulacion text not null,
	foreign key (id_avion) REFERENCES aviones (id),
	foreign key (id_aerolinea) REFERENCES aerolineas (id),
	foreign key (id_aeropuerto_salida ) REFERENCES aeropuertos (id),
	foreign key (id_aeropuerto_llegada) REFERENCES aeropuertos (id)
)

--Doble escala
insert into vuelos(id_avion,id_aerolinea,id_aeropuerto_salida,
id_aeropuerto_llegada,precio,fecha_salida,fecha_llegada,tripulacion,hora_salida,hora_llegada,duracion)
values(1,1,1,2,1000,'24-07-2020' ,'24-07-2020','1-2-3-4-5','11:00:00','15:00:00',60);

insert into vuelos(id_avion,id_aerolinea,id_aeropuerto_salida,
id_aeropuerto_llegada,precio,fecha_salida,fecha_llegada,tripulacion,hora_salida,hora_llegada,duracion)
values(2,1,2,3,1000,'24-07-2020' ,'24-07-2020','6-7-8-9-10','11:00:00','15:00:00',60);

insert into vuelos(id_avion,id_aerolinea,id_aeropuerto_salida,
id_aeropuerto_llegada,precio,fecha_salida,fecha_llegada,tripulacion,hora_salida,hora_llegada,duracion)
values(3,1,3,4,1000,'24-07-2020' ,'24-07-2020','11-12-13-14-15','11:00:00','15:00:00',60);

---Una escala
insert into vuelos(id_avion,id_aerolinea,id_aeropuerto_salida,
id_aeropuerto_llegada,precio,fecha_salida,fecha_llegada,tripulacion,hora_salida,hora_llegada,duracion)
values(4,1,2,5,1000,'24-07-2020' ,'24-07-2020','10-11-12-13-14','11:00:00','21:00:00',60);

--Directo
insert into vuelos(id_avion,id_aerolinea,id_aeropuerto_salida,
id_aeropuerto_llegada,precio,fecha_salida,fecha_llegada,tripulacion,hora_salida,hora_llegada,duracion)
values(5,1,1,5,5000,'24-07-2020','24-07-2020','15-16-17-18-19','11:00:00','21:00:00',60);

update  tripulaciones set estado = 'En servicio'

select * from vuelos

create table historial(
	id serial primary key,
	cedula text not null,
	pais_salida text not null,
	pais_llegada text not null,
	pais_escala1 text ,
	id_pais_escala2 text ,
	fecha_hora timestamp not null,
	duracion integer not null,
	costo integer not null
	
	
);

select * from historial 

INSERT INTO public.historial(
	cedula, pais_salida, pais_llegada, pais_escala1, pais_escala2, fecha_hora, duracion, costo)
	VALUES ('123','Costa Rica','Estados Unidos','Sin primera escala','Sin segunda escala','2020-07-24 15:49:57','1','500');
	
	
INSERT INTO public.historial(
	cedula, pais_salida, pais_llegada, pais_escala1, pais_escala2, fecha_hora, duracion, costo)
	VALUES ('123','Costa Rica','Estados Unidos','Honduras','Mexico','2020-07-24 15:49:57','3','500');
	
	
INSERT INTO public.historial(
	cedula, pais_salida, pais_llegada, pais_escala1, pais_escala2, fecha_hora, duracion, costo)
	VALUES ('123','Costa Rica','Estados Unidos','Mexico','Sin segunda escala','2020-07-24 15:49:57','2','500');		

INSERT INTO public.historial(
	cedula, pais_salida, pais_llegada, pais_escala1, pais_escala2, fecha_hora, duracion, costo)
	VALUES ('123','Costa Rica','España','Sin primera escala','Sin segunda escala','2020-07-24 15:49:57','2','500');		
