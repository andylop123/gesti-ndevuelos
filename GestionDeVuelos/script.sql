create database gestion_vuelos

create table usuarios(
	id serial primary key,
	id_aerolinea integer,
	cedula text not null,
	nombre text not null,
	edad integer,
	contrasena text,
	tipo text not null,
	fecha date not null,
	rol text,
	estado text
)

create table aerolineas(
	id serial primary key,
	nombre text not null,
	ano date not null,
	tipo text not null
)

create table tripulaciones(
	id serial primary key,
	id_aerolinea integer not null,
	cedula text not null,
	nombre text not null,
	fecha date not null,
	rol text not null,
	estado text not null

)

create table aviones(
	id serial primary key,
	id_aerolinea integer not null,
	modelo text not null,
	ano date not null,
	capacidad text not null,
	estado text not null
)

create table aeropuertos(
	id serial primary key,
	iata text not null,
	nombre text not null,
	pais text not null
)






create table vuelos(
	id serial primary key,
	id_aerolinea integer not null,
	precio integer not null,
	fecha_salida date not null,
	aeropuerto_salida text not null,
	fecha_llegada date not null,
	aeropuesto_llegada text not null,
)


