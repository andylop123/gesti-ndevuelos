﻿using GestionDeVuelos.BO;
using GestionDeVuelos.Entidades;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionDeVuelos.DAO
{
    class TripulacionDAO
    {


        public NpgsqlCommand cmd;


        /// <summary>
        /// Inserta un Tripulante
        /// </summary>
        /// <param name="a"> Es un Tripulante</param>
        /// <returns>Inserta un Tripilante en la base de datos</returns>
        public bool insertar(Tripulacion t )
        {
            Conexion con = new Conexion();
            String sql = " INSERT INTO tripulaciones(id_aerolinea, cedula, nombre, fecha, rol, estado) " +
                " VALUES(@id,@ced,@nom,@fec,@rol,@est) ";
            cmd = new NpgsqlCommand(sql, con.con);
            cmd.Parameters.AddWithValue("@id", t.aerolinea.Id);
            cmd.Parameters.AddWithValue("@ced", t.cedula);
            cmd.Parameters.AddWithValue("@nom", t.nombre);
            cmd.Parameters.AddWithValue("@fec", t.nacimiento);
            cmd.Parameters.AddWithValue("@rol", t.rol);
            cmd.Parameters.AddWithValue("@est", "Disponible");

            return cmd.ExecuteNonQuery() == 1;
        }



        /// <summary>
        /// Devuelve un  tripulante de acuerdo a un id
        /// </summary>
        /// <param name="v">filtro para traer un tripulante</param>
        /// <returns> el tripulante</returns>
        public Tripulacion CargarId(int id)
        {
            Conexion con = new Conexion();
            string sql = "SELECT id, id_aerolinea, cedula, nombre, fecha, rol, estado FROM tripulaciones where id = @id";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, con.con);
            cmd.Parameters.AddWithValue("@id",id);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows) {
                if (reader.Read()) {
                    Tripulacion t =  Cargar(reader);
                    con.con.Close();
                    return t;
                }
            }
            return null;

        }



        /// <summary>
        /// Devuelve una lista de tripulantes de acuerdo a un vuelo
        /// </summary>
        /// <param name="v">filtro para traer la lista</param>
        /// <returns> la lista de tripulantes</returns>
        public List<Tripulacion> Seleccionar(Vuelo v)
        {
            Conexion con = new Conexion();
            List<Tripulacion> tripulacion = new List<Tripulacion>();
            string sql = "SELECT id, id_aerolinea, cedula, nombre, fecha, rol, " +
                " estado FROM tripulaciones where id_aerolinea = @id and estado = @est";
            NpgsqlCommand cmd = new NpgsqlCommand(sql,con.con);
            cmd.Parameters.AddWithValue("@id",v.Aerolinea.Id);
            cmd.Parameters.AddWithValue("@est","Disponible");
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows) {
                while (reader.Read()) {
                    Tripulacion t = Cargar(reader);
                    tripulacion.Add(t);
                    ActivarDesactivar(t);
                }
            }
            con.con.Close();
            return tripulacion;
        }


        /// <summary>
        /// Cambia el estado de un tripulante
        /// </summary>
        /// <param name="t">tripulante</param>
        public void ActivarDesactivar(Tripulacion t) {
            Conexion con = new Conexion();
            string sql = "update tripulaciones set estado = @estado where id =@id";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, con.con);
            cmd.Parameters.AddWithValue("@estado",t.estado.Equals("Disponible") == true ? "En servicio":"Disponible");
            cmd.Parameters.AddWithValue("@id",t.Id);
            cmd.ExecuteNonQuery();
        
        }

        /// <summary>
        /// Este metodo consulta una lista a la base de datos
        /// </summary>
        /// <returns> La lista de Tripulacion</returns>
        public List<Tripulacion> ConsultarDatos()
        {
            Conexion con = new Conexion();
            List<Tripulacion> lista = new List<Tripulacion>();
            string sql = "SELECT id, id_aerolinea, cedula, nombre, fecha, rol, estado FROM tripulaciones order by id";


            cmd = new NpgsqlCommand(sql, con.con);
            NpgsqlDataReader dr = cmd.ExecuteReader();

            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    lista.Add(Cargar(dr));
                }
            }
            con.con.Close();

            return lista;
        }

        /// <summary>
        /// Devuelve una lista de tripulantes de acuerdo a un vuelo
        /// </summary>
        /// <param name="nombre">filtro para traer la lista</param>
        /// <returns> la lista de tripulantes</returns>
        public List<Tripulacion> Consulta4(string nombre)
        {
            Conexion con = new Conexion();
            List<Tripulacion> lista = new List<Tripulacion>();
            string sql = " SELECT t.id, t.id_aerolinea, t.cedula, t.nombre, t.fecha, t.rol, t.estado FROM tripulaciones as t inner join aerolineas a on t.id_aerolinea=a.id where a.nombre = '" + nombre + "' order by id";


            cmd = new NpgsqlCommand(sql,con.con);
            NpgsqlDataReader dr = cmd.ExecuteReader();

            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    lista.Add(Cargar(dr));
                }
            }
            con.con.Close();

            return lista;
        }

        /// <summary>
        /// arma el tripulante de acuerdo a los datos de la base de datos
        /// </summary>
        /// <param name="dr"> un dato para leer en la base de datos</param>
        /// <returns> devuelve el tripulante armado</returns>
        private Tripulacion Cargar(NpgsqlDataReader dr)
        {
            Tripulacion t = new Tripulacion()
            {
                Id = dr.GetInt32(0),
                aerolinea = aero(dr.GetInt32(1)),
                cedula = dr.GetString(2),
                nombre = dr.GetString(3),
                nacimiento = dr.GetDateTime(4),
                rol = dr.GetString(5),
                estado = dr.GetString(6)

            };
            return t;
        }



        /// <summary>
        /// Devuelve una arelonia de acuerdo a un id
        /// </summary>
        /// <param name="id"> id de la aerolinea</param>
        /// <returns>la aerolinea con ese id</returns>
        public Aerolinea aero (int id)
        {
            foreach (Aerolinea item in new AerolineaBO().ConsultarDatos())
            {
                if(item.Id == id)
                {
                    return item;
                }
            }
            return null;
        }
    }
}
