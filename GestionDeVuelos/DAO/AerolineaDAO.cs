﻿using GestionDeVuelos.Entidades;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionDeVuelos.DAO
{
    class AerolineaDAO
    {
        public NpgsqlCommand cmd;


        /// <summary>
        /// Inserta una Aerolinea
        /// </summary>
        /// <param name="a">Es una Aerolinea </param>
        /// <returns>Inserta la Aerolinea en la base de datos</returns>
        public bool insertar(Aerolinea a)
        {
            Conexion con = new Conexion();
            String sql = " INSERT INTO public.aerolineas( nombre, tipo, ano) " +
                " VALUES('" + a.nombre + "', '" + a.tipo + "' , '" + a.año + "') ";
            cmd = new NpgsqlCommand(sql, con.con);
            bool respuesta = cmd.ExecuteNonQuery() == 1;
            con.con.Close();
            return respuesta;
        }


        /// <summary>
        /// Este metodo consulta una lista a la base de datos
        /// </summary>
        /// <returns> La lista de arolineas</returns>
        public List<Aerolinea> ConsultarDatos()
        {
            Conexion con = new Conexion();
            List<Aerolinea> lista = new List<Aerolinea>();
            string sql = "SELECT id, nombre, tipo, ano FROM aerolineas order by id";
            

            cmd = new NpgsqlCommand(sql, con.con);
            NpgsqlDataReader dr = cmd.ExecuteReader();

            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    lista.Add(cargar(dr));
                }
            }

            con.con.Close();
            return lista;
        }


        /// <summary>
        /// Selepciona una aerolinea de acuerdo a un id
        /// </summary>
        /// <param name="id">id por el cual se cargara la erolinea</param>
        /// <returns>la aerolinea con ese id</returns>
        public Aerolinea CargarId(int id) {
            Conexion con = new Conexion();
            string sql = "SELECT id, nombre, tipo, ano FROM aerolineas where id = @id";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, con.con);
            cmd.Parameters.AddWithValue("@id",id);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows) {
                if (reader.Read()) {
                    Aerolinea a =  cargar(reader);
                    con.con.Close();
                    return a;
                }
            }
            return null;

        }


        /// <summary>
        /// Arma una aerolinea de acuerdo a los datos de la base de datos
        /// </summary>
        /// <param name="dr">lee datos de la BD</param>
        /// <returns>la aerolinea armada</returns>
        private Aerolinea cargar(NpgsqlDataReader dr)
        {
            Aerolinea a = new Aerolinea()
            {
                Id=dr.GetInt32(0),
                nombre=dr.GetString(1),
                tipo=dr.GetString(2),
                año=dr.GetString(3)
            };
            return a;
        }



    }
}
