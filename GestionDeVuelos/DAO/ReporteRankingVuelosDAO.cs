﻿using GestionDeVuelos.Entidades;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionDeVuelos.DAO
{
    class ReporteRankingVuelosDAO
    {

        /// <summary>
        /// Carga una lista de aviones
        /// </summary>
        /// <param name="a"> aviones de acuerdo a una aerolinea</param>
        /// <returns>los aviones de una aerolinea</returns>
        public List<ReporteRankingVuelos> Seleccionar(Aerolinea aer)
        {
            Conexion con = new Conexion();
            List<ReporteRankingVuelos> r = new List<ReporteRankingVuelos>();

            string sql = "select av.id,av.id_aerolinea,av.modelo,av.capacidad,av.estado,av.ano,count(av) from aviones as av " +
                " join vuelos as v on av.id = v.id_avion where av.id_aerolinea = @id group by av.id";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, con.con);
            cmd.Parameters.AddWithValue("@id",aer.Id);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows) {
                while (reader.Read()) {
                    Avion a = new Avion();

                    a.Id = reader.GetInt32(0);
                    a.Aerolinea = new AerolineaDAO().CargarId(reader.GetInt32(1));
                    a.Modelo = reader.GetString(2);
                    a.Ano = reader.GetString(3);
                    a.Capacidad = reader.GetString(4);
                    a.Estado = reader.GetString(5);
                    
                    r.Add(new ReporteRankingVuelos { Avion = a,Cantidad = reader.GetInt32(6)});
                }
            }
            return r;
        }
    }
}
