﻿using GestionDeVuelos.Entidades;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionDeVuelos.DAO
{
    class HistorialDAO
    {

        /// <summary>
        /// Inserta un vuelo comprado
        /// </summary>
        /// <param name="a"> Es un vuelo comprado</param>
        /// <returns>Inserta un vuelo o varios en la base de datos</returns>
        public bool Insertar(Historial h)
        {
            Conexion con = new Conexion();
            string sql = "INSERT INTO historial( cedula, pais_salida, pais_llegada, pais_escala1, id_pais_escala2, fecha_hora, duracion, costo) " +
    " VALUES(@ced, @ps, @pl, @pe1,@pe2,@fec,@dur,@cos) ";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, con.con);
            cmd.Parameters.AddWithValue("@ced",h.cedula );
            cmd.Parameters.AddWithValue("@ps", h.paisSalida);
            cmd.Parameters.AddWithValue("@pl", h.paisLlegada);
            cmd.Parameters.AddWithValue("@pe1", h.paisEscala1);
            cmd.Parameters.AddWithValue("@pe2", h.paisEscala2);
            cmd.Parameters.AddWithValue("@fec", h.fechaHora);
            cmd.Parameters.AddWithValue("@dur", h.duracion);
            cmd.Parameters.AddWithValue("@cos", h.costo);


            bool res = cmd.ExecuteNonQuery() == 1;
            con.con.Close();
            return res;

        }
    }
}
