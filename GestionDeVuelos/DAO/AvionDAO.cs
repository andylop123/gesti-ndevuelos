﻿using GestionDeVuelos.Entidades;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionDeVuelos.DAO
{
    class AvionDAO
    {


        /// <summary>
        /// Inserta un Avion
        /// </summary>
        /// <param name="a"> Es un Avion</param>
        /// <returns>Inserta un Avion en la base de datos</returns>
        public bool Insertar(Avion a)
        {
            Conexion con = new Conexion();
            string sql = "INSERT INTO aviones(id_aerolinea, modelo, ano, capacidad, estado) " +
                " VALUES(@id_aero,@mod, @ano,@cap, @est)";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, con.con);
            cmd.Parameters.AddWithValue("@id_aero", a.Aerolinea.Id);
            cmd.Parameters.AddWithValue("@mod", a.Modelo);
            cmd.Parameters.AddWithValue("@ano", a.Ano);
            cmd.Parameters.AddWithValue("@cap", a.Capacidad);
            cmd.Parameters.AddWithValue("@est", "Disponible");
            bool res = cmd.ExecuteNonQuery() == 1;
            con.con.Close();
            return res;

        }


        /// <summary>
        /// carga un avion de acuerdo a unos datos
        /// </summary>
        /// <param name="vuelo"> carga un avion de acuerdo a unos datos</param>
        /// <returns> devuelve el avion de acuerdo a esos datos </returns>
        public Avion Validar(Vuelo vuelo)
        {
            Conexion con = new Conexion();
            MessageBox.Show(vuelo.FechaSalida+"");
            string sql = "SELECT avion.id, avion.id_aerolinea, modelo, capacidad, estado, ano FROM aviones as " +
                " avion join vuelos as v on avion.id = v.id_avion where (@fech between v.fecha_salida and v.fecha_llegada) and" +
                " avion.id = @id group by avion.id";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, con.con);
            cmd.Parameters.AddWithValue("@id", vuelo.Avion.Id);
            cmd.Parameters.AddWithValue("@fech",DateTime.Parse(vuelo.FechaSalida.ToString("yyyy/MM/dd")));
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows) {
                if (reader.Read()) {
                    return Cargar(reader);
                }
            }
            return null;
        }



        /// <summary>
        /// carga un avion de acuerdo a unos datos
        /// </summary>
        /// <param name="vuelo"> carga un avion de acuerdo a unos datos</param>
        /// <returns> devuelve el avion de acuerdo a esos datos </returns>
        public Avion CargarAerolinea(Vuelo v)
        {
            Conexion con = new Conexion();
            string sql = "select id,id_aerolinea,modelo,ano,capacidad,estado from aviones " +
                " where id_aerolinea = @id and estado = @est";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, con.con);
            cmd.Parameters.AddWithValue("@id", v.Aerolinea.Id);
            cmd.Parameters.AddWithValue("@est", "Disponible");
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                if (reader.Read())
                {

                    Avion a =  Cargar(reader);
                    con.con.Close();
                    ActivarDesactivar(a);
                    return a;
                }
            }
            
            return null;
        }


        /// <summary>
        /// cambia el estado de un avion
        /// </summary>
        /// <param name="avion"> avion al cual se le cambiara el estado</param>
        public void ActivarDesactivar(Avion avion)
        {
            Conexion con = new Conexion();
            string sql = "update aviones set estado = @est where id = @id";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, con.con);
            cmd.Parameters.AddWithValue("@est",avion.Estado.Equals("Disponible") == true?"En uso":"Disponible");
            cmd.Parameters.AddWithValue("@id", avion.Id);
            cmd.ExecuteNonQuery();
        }

        /// <summary>
        /// Carga un Avion por un filto
        /// </summary>
        /// <param name="filtro"> Filtro por el cual se va cargar el Avion</param>
        /// <returns>Devuelve una lista de Aviones</returns>
        public List<Avion> Seleccionar(string filtro)
        {
            Conexion con = new Conexion();
            List<Avion> aviones = new List<Avion>();
            string sql = "select id,id_aerolinea,modelo,ano,capacidad,estado from aviones" +
                " where modelo like(@mod) order by id_aerolinea";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, con.con);
            cmd.Parameters.AddWithValue("@mod", filtro + '%');
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    aviones.Add(Cargar(reader));
                }
            }
            con.con.Close();
            return aviones;
        }


        /// <summary>
        /// Arma un Avion con los datos de la BD
        /// </summary>
        /// <param name="reader">lee los datos de la BD</param>
        /// <returns> el avion armado</returns>
        public Avion Cargar(NpgsqlDataReader reader)
        {
            Avion a = new Avion
            {
                Id = reader.GetInt32(0),
                Aerolinea = new AerolineaDAO().CargarId(reader.GetInt32(1)),
                Modelo = reader.GetString(2),
                Ano = reader.GetString(3),
                Capacidad = reader.GetString(4),
                Estado = reader.GetString(5)
            };
            return a;
        }



        /// <summary>
        /// Carga una lista de modelos de aviones
        /// </summary>
        /// <returns> devuelve la lista de aviones</returns>
        public List<string> Modelos()
        {
            Conexion con = new Conexion();
            List<string> modelos = new List<string>();
            string sql = "select modelo from aviones";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, con.con);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    modelos.Add(reader.GetString(0));
                }
            }
            con.con.Close();
            return modelos;

        }


        /// <summary>
        /// Devulve un avion de acuerdo a un id
        /// </summary>
        /// <param name="id">id por el cual se va a cargar el avion</param>
        /// <returns> carga el avion de acuerdo al id</returns>
        public Avion CargarId(int id)
        {
            Conexion con = new Conexion();
            string sql = "SELECT id, id_aerolinea, modelo, capacidad, estado, ano FROM aviones where id = @id";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, con.con);
            cmd.Parameters.AddWithValue("@id", id);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                if (reader.Read())
                {
                    Avion a =  Cargar(reader);
                    con.con.Close();
                    return a;
                }
            }
            return null;
        }
    }
}
