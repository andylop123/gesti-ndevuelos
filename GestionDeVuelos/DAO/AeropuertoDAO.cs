﻿using GestionDeVuelos.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;

namespace GestionDeVuelos.DAO
{
    class AeropuertoDAO
    {

        /// <summary>
        /// Inserta un Aeropuerto
        /// </summary>
        /// <param name="a"> Es un aeropuerto</param>
        /// <returns>Inserta un aeropuerto en la base de datos</returns>
        public bool Insertar(Aeropuerto a)
        {
            Conexion con = new Conexion();
            string sql = "INSERT INTO public.aeropuertos(iata, nombre, pais) VALUES(@ia, @nom, @pais)";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, con.con);
            cmd.Parameters.AddWithValue("@ia", a.Iata);
            cmd.Parameters.AddWithValue("@nom", a.Nombre);
            cmd.Parameters.AddWithValue("@pais", a.Pais);
            bool res = cmd.ExecuteNonQuery() == 1;
            con.con.Close();
            return res;

        }


        /// <summary>
        /// Carga un aeropuerto por un filto
        /// </summary>
        /// <param name="filtro"> Filtro por el cual se va cargar el aeropuerto</param>
        /// <returns>Devuelve el aeropuerto</returns>
        public Aeropuerto CargarAeropuerto(string filtro)
        {
            Conexion con = new Conexion();
            string sql = "Select id, iata, nombre, pais FROM aeropuertos " +
                " where lower(iata) like lower(@ia) or lower(nombre) like lower(@nom)";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, con.con);
            cmd.Parameters.AddWithValue("@ia", filtro + '%');
            cmd.Parameters.AddWithValue("@nom", filtro + '%');
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                if (reader.Read())
                {
                    Aeropuerto a = Cargar(reader);
                    con.con.Close();
                    return a;
                }
            }
            con.con.Close();
            return null;
        }



        /// <summary>
        /// Carga una lista con un aeropuerto por un filto
        /// </summary>
        /// <param name="filtro"> Filtro por el cual se va cargar el aeropuerto</param>
        /// <returns>Devuelve la lista de aeropuertos</returns>
        public List<Aeropuerto> Seleccionar(string filtro)
        {
            Conexion con = new Conexion();
            List<Aeropuerto> aviones = new List<Aeropuerto>();
            string sql = "SELECT id, iata, nombre, pais FROM aeropuertos where iata like(@ia)";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, con.con);
            cmd.Parameters.AddWithValue("@ia", filtro + '%');
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    aviones.Add(Cargar(reader));
                }
            }
            con.con.Close();
            return aviones;
        }

        /// <summary>
        /// Carga un aeropuerto por un id
        /// </summary>
        /// <param name="id"> id por el cual se va cargar el aeropuerto</param>
        /// <returns>Devuelve el aeropuerto</returns>
        public Aeropuerto CargarId(int id)
        {
            Conexion con = new Conexion();
            Aeropuerto a = new Aeropuerto();
            string sql = "SELECT id, iata, nombre, pais FROM aeropuertos where id = @id";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, con.con);
            cmd.Parameters.AddWithValue("@id", id);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                if (reader.Read())
                {
                    a = Cargar(reader);
                    con.con.Close();
                    return a;
                }
            }
            return null;
        }

        /// <summary>
        /// Arma un aeropuerto con los datos de la BD
        /// </summary>
        /// <param name="reader">lee los datos de la BD</param>
        /// <returns> el aeropuerto armado</returns>
        public Aeropuerto Cargar(NpgsqlDataReader reader)
        {
            Aeropuerto a = new Aeropuerto
            {
                Id = reader.GetInt32(0),
                Iata = reader.GetString(1),
                Nombre = reader.GetString(2),
                Pais = reader.GetString(3)
            };
            return a;

        }
    }
}
