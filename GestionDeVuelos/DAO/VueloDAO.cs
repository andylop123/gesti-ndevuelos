﻿using GestionDeVuelos.Entidades;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionDeVuelos.DAO
{
    class VueloDAO
    {
        /// <summary>
        /// Este metodo inserta un vuelo en la base de datos
        /// </summary>
        /// <param name="v">Este parametro representa el vuelo a insertar</param>
        /// <returns>Devuelve true si se logro insertar</returns>
        public bool Insertar(Vuelo v)
        {
            Conexion con = new Conexion();
            string sql = "INSERT INTO vuelos(id_aerolinea, id_aeropuerto_salida, id_aeropuerto_llegada,id_avion, " +
                " precio, fecha_salida, fecha_llegada, tripulacion,hora_salida,hora_llegada,duracion) " +
                " VALUES(@aerolinea, @aeropuertoSal, @aeropuertoLleg,@id_avion, @prec, @fechSal, @fechLleg, @trip," +
                " @horaSalida,@horaLlegada,@duracion)";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, con.con);
            cmd.Parameters.AddWithValue("@aerolinea", v.Aerolinea.Id);
            cmd.Parameters.AddWithValue("@aeropuertoSal", v.AeropuertoSalida.Id);
            cmd.Parameters.AddWithValue("@aeropuertoLleg", v.AeropuertoLlegada.Id);
            cmd.Parameters.AddWithValue("@id_avion", v.Avion.Id);
            cmd.Parameters.AddWithValue("@prec", v.Precio);
            cmd.Parameters.AddWithValue("@fechSal", v.FechaSalida);
            cmd.Parameters.AddWithValue("@fechLleg", v.FechaLlegada);
            cmd.Parameters.AddWithValue("@trip", v.Tripulacion);
            cmd.Parameters.AddWithValue("@horaSalida", Convert.ToDateTime( v.HoraSalida));
            cmd.Parameters.AddWithValue("@horaLlegada", Convert.ToDateTime(v.HoraLLegada));
            cmd.Parameters.AddWithValue("@duracion", v.Duracion);
            bool res = cmd.ExecuteNonQuery() == 1;
            con.con.Close();
            return res;
        }

        /// <summary>
        /// Este metodo devuelve un vuelo si ese vuelo cumple los requisitos
        /// </summary>
        /// <param name="v">Este parametro representa el vuelo a buscar</param>
        /// <returns>Devuelve el vuelo si fue encontrado</returns>
        public Vuelo CargarVueloAeroFecha(Vuelo v)
        {
            Conexion con = new Conexion();    
            string sql = "SELECT v.id, v.id_aerolinea, v.id_aeropuerto_salida, v.id_aeropuerto_llegada, v.precio, v.fecha_salida, v.fecha_llegada, v.tripulacion, " +
                " v.id_avion, v.duracion,v.hora_salida, v.hora_llegada from vuelos as v " +
                " join aeropuertos as aero_sal on v.id_aeropuerto_salida = aero_sal.id " +
                " join aeropuertos as aero_lleg on v.id_aeropuerto_llegada = aero_lleg.id  " +
                " where id_aerolinea = @id and v.fecha_salida = @fech and aero_sal.nombre = @AeroSal and aero_lleg.nombre = @AeroLleg order by precio";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, con.con);
            cmd.Parameters.AddWithValue("@id",v.Aerolinea.Id);
            cmd.Parameters.AddWithValue("@fech", DateTime.Parse(v.FechaSalida.ToString("dd/MM/yyyy")));
            cmd.Parameters.AddWithValue("@AeroSal",v.AeropuertoSalida.Nombre);
            cmd.Parameters.AddWithValue("@AeroLleg",v.AeropuertoLlegada.Nombre);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows) {
                if (reader.Read()) {
                    return Cargar(reader);
                }
            }
            return null;
        }

        /// <summary>
        /// Este metodo devuelve todos los vuelos registrados en la base de datos
        /// </summary>
        /// <returns>Devuelve una lista de vueloss</returns>
        public List<Vuelo> Seleccionar()
        {
            Conexion con = new Conexion();
            List<Vuelo> vuelos = new List<Vuelo>();
            string sql = " SELECT v.id, v.id_aerolinea, v.id_aeropuerto_salida, v.id_aeropuerto_llegada, " +
                " v.precio, v.fecha_salida, v.fecha_llegada, v.tripulacion,v.id_avion,v.duracion,hora_salida,hora_llegada FROM vuelos v ";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, con.con);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    vuelos.Add(Cargar(reader));
                }
            }
            con.con.Close();
            return vuelos;
        }

        /// <summary>
        /// Este metodo devuelve un vuelo por id
        /// </summary>
        /// <param name="id">Este paramtro representa el id del vuelo a buscar</param>
        /// <returns></returns>
        public Vuelo SeleccionarID(int id)
        {
            Conexion con = new Conexion();
            string sql = " SELECT v.id, v.id_aerolinea, v.id_aeropuerto_salida, v.id_aeropuerto_llegada, " +
                " v.precio, v.fecha_salida, v.fecha_llegada, v.tripulacion,v.id_avion,v.duracion,hora_salida,hora_llegada FROM vuelos v where v.id = @id";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, con.con);
            cmd.Parameters.AddWithValue("@id", id);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    return Cargar(reader);
                }
            }
            con.con.Close();
            return null;
        }

        /// <summary>
        /// Este metodo devuelve un vuelo
        /// </summary>
        /// <param name="reader">Representa el vuelo a combertir</param>
        /// <returns>Devuelve un vuelo</returns>
        public Vuelo Cargar(NpgsqlDataReader reader)
        {

            Vuelo v = new Vuelo();

            v.Id = reader.GetInt32(0);
            v.Aerolinea = new AerolineaDAO().CargarId(reader.GetInt32(1));
            v.AeropuertoSalida = new AeropuertoDAO().CargarId(reader.GetInt32(2));
            v.AeropuertoLlegada = new AeropuertoDAO().CargarId(reader.GetInt32(3));
            v.Precio = reader.GetInt32(4);
            v.FechaSalida = reader.GetDateTime(5);
            v.FechaLlegada = reader.GetDateTime(6);
            v.Tripulacion = reader.GetString(7);
            v.Avion = new AvionDAO().CargarId(reader.GetInt32(8));
            v.Duracion = reader.GetInt32(9);
            v.HoraSalida= reader.GetTimeSpan(10).ToString();
            v.HoraLLegada = reader.GetTimeSpan(11).ToString();
            return v;


        }

        /// <summary>
        /// Este metodo devuelve una lista de vuelos de posibles escalas
        /// </summary>
        /// <param name="v">Este parametro representa los vuelos a buscar</param>
        /// <returns>Devuelve una lista de escalas posibles</returns>
        public List<Vuelo> Escala(Vuelo v)
        {
            Conexion con = new Conexion();
            List<Vuelo> vuelos = new List<Vuelo>();
            string sql = "SELECT v.id, v.id_aerolinea, v.id_aeropuerto_salida, v.id_aeropuerto_llegada, " +
                " v.precio, v.fecha_salida, v.fecha_llegada, v.tripulacion,v.id_avion,v.duracion,hora_salida,hora_llegada FROM vuelos as v join aeropuertos as aero_sal " +
                " on v.id_aeropuerto_salida = aero_sal.id join aeropuertos as aero_lleg on v.id_aeropuerto_llegada = aero_lleg.id " +
                " where (v.fecha_salida between @salida and  @posible ) and " +
                " lower(aero_lleg.nombre) like lower(@aerLleg)";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, con.con);
            cmd.Parameters.AddWithValue("@salida", DateTime.Parse(v.FechaSalida.ToString("dd/MM/yyyy")));
            cmd.Parameters.AddWithValue("@posible", DateTime.Parse(v.FechaSalida.ToString("dd/MM/yyyy")).AddDays(1));
            cmd.Parameters.AddWithValue("@aerLleg", v.AeropuertoLlegada.Nombre);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    vuelos.Add(Cargar(reader));
                }
            }
            con.con.Close();
            return vuelos;
        }

        /// <summary>
        /// Este metodo devuelve un lista de vuelos
        /// </summary>
        /// <param name="v">Este parametro presenta el vuelo a buscar</param>
        /// <returns>Devuelve una lista de vuelos</returns>
        public List<Vuelo> CargarVuelosSimple(Vuelo v)
        {

            Conexion con = new Conexion();
            List<Vuelo> vuelos = new List<Vuelo>();
            string sql = "SELECT v.id, v.id_aerolinea, v.id_aeropuerto_salida, v.id_aeropuerto_llegada, " +
                " v.precio, v.fecha_salida, v.fecha_llegada, v.tripulacion,v.id_avion,v.duracion,hora_salida,hora_llegada FROM vuelos as v join aeropuertos as aero_sal " +
                " on v.id_aeropuerto_salida = aero_sal.id join aeropuertos as aero_lleg on v.id_aeropuerto_llegada = aero_lleg.id " +
                " where fecha_salida = @fechSal and lower(aero_sal.nombre) like lower(@aerSal) and " +
                " lower(aero_lleg.nombre) like lower(@aerLleg) ";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, con.con);
            cmd.Parameters.AddWithValue("@fechSal", DateTime.Parse(v.FechaSalida.ToString("dd/MM/yyyy")));
            cmd.Parameters.AddWithValue("@aerSal", v.AeropuertoSalida.Nombre + '%');
            cmd.Parameters.AddWithValue("@aerLleg", v.AeropuertoLlegada.Nombre + '%');

            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    vuelos.Add(Cargar(reader));
                }

            }
            con.con.Close();
            return vuelos;
        }


        /// <summary>
        /// Este metodo devuelve una lista de vuelos 
        /// </summary>
        /// <param name="v">Este parametro representa el vuelo a buscar</param>
        /// <param name="r">Representa el rango de dias a buscar</param>
        /// <param name="tipo">Representa como es el orden de los vuelos</param>
        /// <returns>Devuelve una lista de vuelos</returns>
        public List<Vuelo> CargarVuelosInteligentes(Vuelo v, int r, bool tipo)
        {
            string sql = "";

            Conexion con = new Conexion();
            List<Vuelo> vuelos = new List<Vuelo>();

            if (tipo)
            {
                sql = " SELECT v.id, v.id_aerolinea, v.id_aeropuerto_salida, v.id_aeropuerto_llegada, " +
   " v.precio, v.fecha_salida, v.fecha_llegada, v.tripulacion,v.id_avion,v.duracion,hora_salida,hora_llegada FROM vuelos v join aeropuertos  aero_sal " +
      " on v.id_aeropuerto_salida = aero_sal.id join aeropuertos  aero_lleg on v.id_aeropuerto_llegada = aero_lleg.id " +
     " where lower(aero_sal.nombre) like lower(@aerSal) and " +
      " lower(aero_lleg.nombre) like lower(@aerLleg) and " +
     " (fecha_salida BETWEEN @fechSal1 and @fechSal2) and (fecha_llegada BETWEEN @fechlleg1 and @fechlleg2) order by v.precio desc ";



            }
            else
            {

                sql = " SELECT v.id, v.id_aerolinea, v.id_aeropuerto_salida, v.id_aeropuerto_llegada, " +
   " v.precio, v.fecha_salida, v.fecha_llegada, v.tripulacion,v.id_avion,v.duracion,hora_salida,hora_llegada FROM vuelos v join aeropuertos  aero_sal " +
      " on v.id_aeropuerto_salida = aero_sal.id join aeropuertos  aero_lleg on v.id_aeropuerto_llegada = aero_lleg.id " +
     " where lower(aero_sal.nombre) like lower(@aerSal) and " +
      " lower(aero_lleg.nombre) like lower(@aerLleg) order by v.precio desc ";



            }


            NpgsqlCommand cmd = new NpgsqlCommand(sql, con.con);
            cmd.Parameters.AddWithValue("@aerSal", v.AeropuertoSalida.Nombre + '%');
            cmd.Parameters.AddWithValue("@aerLleg", v.AeropuertoLlegada.Nombre + '%');
            cmd.Parameters.AddWithValue("@fechSal1", v.FechaSalida.AddDays(-r));
            cmd.Parameters.AddWithValue("@fechSal2", v.FechaSalida.AddDays(r));
            cmd.Parameters.AddWithValue("@fechlleg1", v.FechaLlegada.AddDays(-r));
            cmd.Parameters.AddWithValue("@fechlleg2", v.FechaLlegada.AddDays(r));


            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    vuelos.Add(Cargar(reader));
                }

            }
            con.con.Close();
            return vuelos;
        }






        /// <summary>
        /// Este metodo devuelve una lista de vuelos
        /// </summary>
        /// <param name="v">Representa el vuelo a buscar</param>
        /// <param name="r">Representa el rango de fechas a buscar</param>
        /// <param name="tipo">Representa el orden de la lista</param>
        /// <returns>Devuelve una lsita de vuelos</returns>
        public List<Vuelo> CargarVuelosInteligentesEscala(Vuelo v, int r, bool tipo)
        {
            string sql = "";

            Conexion con = new Conexion();
            List<Vuelo> vuelos = new List<Vuelo>();

            if (tipo)
            {
                sql = " SELECT v.id, v.id_aerolinea, v.id_aeropuerto_salida, v.id_aeropuerto_llegada, " +
   " v.precio, v.fecha_salida, v.fecha_llegada, v.tripulacion,v.id_avion,v.duracion,hora_salida,hora_llegada FROM vuelos v join aeropuertos  aero_sal " +
      " on v.id_aeropuerto_salida = aero_sal.id join aeropuertos  aero_lleg on v.id_aeropuerto_llegada = aero_lleg.id " +
     " where lower(aero_sal.nombre) like lower(@aerSal) and " +
     " (fecha_salida BETWEEN @fechSal1 and @fechSal2) order by v.precio desc ";



            }
            else
            {

                sql = " SELECT v.id, v.id_aerolinea, v.id_aeropuerto_salida, v.id_aeropuerto_llegada, " +
   " v.precio, v.fecha_salida, v.fecha_llegada, v.tripulacion,v.id_avion,v.duracion,hora_salida,hora_llegada FROM vuelos v join aeropuertos  aero_sal " +
      " on v.id_aeropuerto_salida = aero_sal.id join aeropuertos  aero_lleg on v.id_aeropuerto_llegada = aero_lleg.id " +
     " where lower(aero_lleg.nombre) like lower(@aerLleg) order by v.precio desc ";



            }


            NpgsqlCommand cmd = new NpgsqlCommand(sql, con.con);
            cmd.Parameters.AddWithValue("@aerSal", v.AeropuertoSalida.Nombre + '%');
            cmd.Parameters.AddWithValue("@aerLleg", v.AeropuertoLlegada.Nombre + '%');
            cmd.Parameters.AddWithValue("@fechSal1", v.FechaSalida.AddDays(-r));
            cmd.Parameters.AddWithValue("@fechSal2", v.FechaSalida.AddDays(r));



            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    vuelos.Add(Cargar(reader));
                }

            }
            con.con.Close();
            return vuelos;
        }



        /// <summary>
        /// Este metodo devuelve una lista de vuelos
        /// </summary>
        /// <param name="v">Representa el vuelo a buscar</param>
        /// <returns>Devuelve una lista de vuelos</returns>
        public List<Vuelo> CargarVuelosRango(Vuelo v)
        {
            Conexion con = new Conexion();
            List<Vuelo> vuelos = new List<Vuelo>();
            string sql = "SELECT id, id_aerolinea, id_aeropuerto_salida, id_aeropuerto_llegada, precio, fecha_salida, " +
                " fecha_llegada, tripulacion, id_avion,duracion,hora_salida,hora_llegada FROM vuelos where fecha_salida between @fechS and @fechLleg";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, con.con);
            cmd.Parameters.AddWithValue("@fechS", Convert.ToDateTime(v.FechaSalida.ToString("dd/MM/yyyy")));
            cmd.Parameters.AddWithValue("@fechLleg", Convert.ToDateTime(v.FechaLlegada.ToString("dd/MM/yyyy")));
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    vuelos.Add(Cargar(reader));
                }
            }
            con.con.Close();
            return vuelos; ;
        }

        /// <summary>
        /// Este metodo devuelve una lista de vuelos segun su aerolinea
        /// </summary>
        /// <param name="v">Representa los vuelos a buscar</param>
        /// <returns>Devuelve una lista de vuelos</returns>
        public List<Vuelo> CargarVuelosAerolinea(Aerolinea v)
        {
            Conexion con = new Conexion();
            List<Vuelo> vuelos = new List<Vuelo>();
            string sql = " SELECT v.id, id_aerolinea, id_aeropuerto_salida, id_aeropuerto_llegada, precio, fecha_salida, " +
                " fecha_llegada, tripulacion, id_avion,duracion,hora_salida,hora_llegada FROM vuelos v INNER join aerolineas a on v.id_aerolinea = a.id  where a.id = '" + v.Id + "' ";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, con.con);

            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    vuelos.Add(Cargar(reader));
                }
            }
            con.con.Close();
            return vuelos; ;
        }

        public Vuelo CargarId(Vuelo v) {
            Conexion con = new Conexion();
            string sql = "SELECT id, id_aerolinea, id_aeropuerto_salida, id_aeropuerto_llegada, precio, fecha_salida, " +
                " fecha_llegada, tripulacion, id_avion,duracion,hora_salida,hora_llegada FROM vuelos where id = @id";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, con.con);
            cmd.Parameters.AddWithValue("@id",v.Id);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows) {
                if (reader.Read()) {
                    return Cargar(reader);
                }
            }

            con.con.Close();
            return null;

        }



    }
}
