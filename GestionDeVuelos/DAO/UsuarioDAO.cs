﻿using GestionDeVuelos.Entidades;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace GestionDeVuelos.DAO
{
    class UsuarioDAO
    {
        /// <summary>
        /// Inserta un Usuario
        /// </summary>
        /// <param name="usu"> Es un Usuario</param>
        /// <returns>Inserta un usuario en la base de datos</returns>
        public bool Insertar(Usuario usu) {
            Conexion con = new Conexion();
            string sql = "INSERT INTO usuarios(cedula, nombre, edad, contrasena, tipo)" +
                "VALUES(@ced,@nom, @edad,@contra,@tipo)";
            NpgsqlCommand cmd = new NpgsqlCommand(sql, con.con);
            cmd.Parameters.AddWithValue("@ced",usu.Cedula);
            cmd.Parameters.AddWithValue("@nom",usu.Nombre);
            cmd.Parameters.AddWithValue("@edad",usu.Edad);
            cmd.Parameters.AddWithValue("@contra",usu.Contrasena);
            cmd.Parameters.AddWithValue("@tipo","Pasajero");
            return cmd.ExecuteNonQuery() == 1;

        
        }


        /// <summary>
        /// Este metodo bucca un usuario en la base de datos
        /// </summary>
        /// <param name="usu">usuario a buscar</param>
        /// <returns>devuele el usuario</returns>
        public Usuario Login(Usuario usu)
        {
            Conexion con = new Conexion();
            string sql = "select id,cedula,nombre,edad,contrasena,tipo from " +
                "usuarios where cedula = @ced and contrasena = upper(@contra)";
            NpgsqlCommand cmd = new NpgsqlCommand(sql,con.con);
            cmd.Parameters.AddWithValue("@ced",usu.Cedula);
            cmd.Parameters.AddWithValue("@contra",usu.Contrasena);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows) {
                if (reader.Read()) {
                    return Cargar(reader);
                }
            }
            return null;
        }

        /// <summary>
        /// carga un usuario de acuerdo a los datos de la base de datos
        /// </summary>
        /// <param name="reader"> lee datos de la base de datos</param>
        /// <returns>un usuario</returns>
        public Usuario Cargar(NpgsqlDataReader reader) {
            Usuario usu = new Usuario
            {
                Id = reader.GetInt32(0),
                Cedula = reader.GetString(1),
                Nombre = reader.GetString(2),
                Edad = reader.GetInt32(3),
                Contrasena = reader.GetString(4),
                Tipo = reader.GetString(5)

            };
            return usu;
        }


        /// <summary>
        /// encrypta las contraseñas para el login
        /// </summary>
        /// <param name="contra"> contraseña a encriptar</param>
        /// <returns>un arreglo de bits</returns>
        public string contraMD5(string contra) {
            MD5 md5 = MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(contra);
            byte[] hash = md5.ComputeHash(inputBytes);
            return BitConverter.ToString(hash).Replace("-", "");
        
        }
    }
}
