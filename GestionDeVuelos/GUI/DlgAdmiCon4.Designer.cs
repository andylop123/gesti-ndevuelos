﻿namespace GestionDeVuelos.GUI
{
    partial class DlgAdmiCon4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cbAerolinea = new System.Windows.Forms.ComboBox();
            this.dtTriplucion = new System.Windows.Forms.DataGridView();
            this.t = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cedula = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nacimiento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.aero = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Rol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Estado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtTriplucion)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(43)))), ((int)(((byte)(49)))));
            this.panel1.Controls.Add(this.cbAerolinea);
            this.panel1.Controls.Add(this.dtTriplucion);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel1.Location = new System.Drawing.Point(-155, -26);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1155, 545);
            this.panel1.TabIndex = 1;
            // 
            // cbAerolinea
            // 
            this.cbAerolinea.FormattingEnabled = true;
            this.cbAerolinea.Location = new System.Drawing.Point(516, 82);
            this.cbAerolinea.Name = "cbAerolinea";
            this.cbAerolinea.Size = new System.Drawing.Size(261, 31);
            this.cbAerolinea.TabIndex = 17;
            this.cbAerolinea.SelectedIndexChanged += new System.EventHandler(this.cbAerolinea_SelectedIndexChanged);
            // 
            // dtTriplucion
            // 
            this.dtTriplucion.AllowUserToAddRows = false;
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            this.dtTriplucion.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dtTriplucion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtTriplucion.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.t,
            this.Cedula,
            this.nombre,
            this.nacimiento,
            this.aero,
            this.Rol,
            this.Estado});
            this.dtTriplucion.Location = new System.Drawing.Point(224, 157);
            this.dtTriplucion.Name = "dtTriplucion";
            this.dtTriplucion.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dtTriplucion.RowTemplate.Height = 24;
            this.dtTriplucion.Size = new System.Drawing.Size(836, 356);
            this.dtTriplucion.TabIndex = 1;
            // 
            // t
            // 
            this.t.HeaderText = "t";
            this.t.MinimumWidth = 6;
            this.t.Name = "t";
            this.t.Visible = false;
            this.t.Width = 125;
            // 
            // Cedula
            // 
            this.Cedula.HeaderText = "Cédula";
            this.Cedula.MinimumWidth = 6;
            this.Cedula.Name = "Cedula";
            this.Cedula.Width = 125;
            // 
            // nombre
            // 
            this.nombre.HeaderText = "Nombre";
            this.nombre.MinimumWidth = 6;
            this.nombre.Name = "nombre";
            this.nombre.Width = 125;
            // 
            // nacimiento
            // 
            this.nacimiento.HeaderText = "Fecha de Nacimiento";
            this.nacimiento.MinimumWidth = 6;
            this.nacimiento.Name = "nacimiento";
            this.nacimiento.Width = 125;
            // 
            // aero
            // 
            this.aero.HeaderText = "Aerolinea";
            this.aero.MinimumWidth = 6;
            this.aero.Name = "aero";
            this.aero.Width = 125;
            // 
            // Rol
            // 
            this.Rol.HeaderText = "Rol";
            this.Rol.MinimumWidth = 6;
            this.Rol.Name = "Rol";
            this.Rol.Width = 125;
            // 
            // Estado
            // 
            this.Estado.HeaderText = "Estado";
            this.Estado.MinimumWidth = 6;
            this.Estado.Name = "Estado";
            this.Estado.Width = 125;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(220, 90);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(266, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Seleccione una aerolinea:";
            // 
            // DlgAdmiCon4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1003, 517);
            this.Controls.Add(this.panel1);
            this.Name = "DlgAdmiCon4";
            this.Text = "DlgAdmiCon4";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.DlgAdmiCon4_FormClosed);
            this.Load += new System.EventHandler(this.DlgAdmiCon4_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtTriplucion)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dtTriplucion;
        private System.Windows.Forms.DataGridViewTextBoxColumn t;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cedula;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn nacimiento;
        private System.Windows.Forms.DataGridViewTextBoxColumn aero;
        private System.Windows.Forms.DataGridViewTextBoxColumn Rol;
        private System.Windows.Forms.DataGridViewTextBoxColumn Estado;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbAerolinea;
    }
}