﻿using GestionDeVuelos.BO;
using GestionDeVuelos.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionDeVuelos.GUI
{
    public partial class DlgVuelos : Form
    {
        private Form parent;
        public DlgVuelos(Form parent)
        {
            InitializeComponent();
            this.parent = parent;
            CargarDatos();
            CargarData();
        }

        /// <summary>
        /// Este metodo cargar las aerolineas y los aeropuertos 
        /// </summary>
        private void CargarData()
        {
            foreach (Aerolinea a in new AerolineaBO().ConsultarDatos())
            {
                cbAerolineas.Items.Add(a);
            }

            foreach (Aeropuerto a in new AeropuertoBO().Seleccionar(""))
            {
                cbAeroLleg.Items.Add(a);
                cbAeroSal.Items.Add(a);
            }

        }

        /// <summary>
        /// Este metodo carga los vuelos que existen en la base de datos
        /// </summary>
        private void CargarDatos()
        {
            try
            {
                dataGridView1.Rows.Clear();
                foreach (Vuelo v in new VueloBO().Cargar())
                {
                    dataGridView1.Rows.Add(v.Aerolinea.nombre, v.FechaSalida, v.FechaLlegada, v.AeropuertoSalida.Nombre,
                        v.AeropuertoLlegada.Nombre, v.Precio, v.Avion.Modelo);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.StackTrace);
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {


                Aerolinea a = (Aerolinea)cbAerolineas.SelectedItem;
                int precio = Int32.Parse(txtPrecio.Text.Trim());
                DateTime fechaSal = dateSal.Value;
                DateTime horaSal = dtHoraSal.Value;
                DateTime fechaLleg = dateLleg.Value;
                DateTime horaLLeg = dtHoraLLeg.Value;
                Aeropuerto aeroSal = (Aeropuerto)cbAeroSal.SelectedItem;
                Aeropuerto aeroLleg = (Aeropuerto)cbAeroLleg.SelectedItem;
                int duracion = Int32.Parse(txtDur.Text.Trim());
                Vuelo v = new Vuelo
                {
                    Aerolinea = a,
                    Precio = precio,
                    FechaSalida = fechaSal,
                    HoraSalida = horaSal.ToString(),
                    FechaLlegada = fechaLleg,
                    HoraLLegada = horaLLeg.ToString(),
                    AeropuertoSalida = aeroSal,
                    AeropuertoLlegada = aeroLleg,
                    Duracion = duracion,

                };
 

                if (new VueloBO().Insertar(v))
                {
                    Limpiar();
                    CargarDatos();
                    MessageBox.Show("Vuelo registrado con éxito");

                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }




        }

        /// <summary>
        /// Este metodo limpia los datos
        /// </summary>
        private void Limpiar()
        {
            txtPrecio.Clear();
            cbAerolineas.Text = "";
            cbAeroSal.Text = "";
            cbAeroLleg.Text = "";

        }

        private void DlgVuelos_Load(object sender, EventArgs e)
        {

        }

        private void DlgVuelos_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (parent != null)
            {
                parent.Show();
            }
        }
    }
}
