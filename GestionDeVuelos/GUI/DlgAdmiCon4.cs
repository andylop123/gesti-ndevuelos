﻿using GestionDeVuelos.BO;
using GestionDeVuelos.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionDeVuelos.GUI
{
    public partial class DlgAdmiCon4 : Form
    {
        private Form parent;
        public DlgAdmiCon4(Form parent)
        {
            InitializeComponent();
            dtTriplucion.ForeColor = Color.Black;
            this.parent = parent;
        }
        private void aerolinea()
        {
            foreach (Aerolinea a in new AerolineaBO().ConsultarDatos())
            {
                cbAerolinea.Items.Add(a);
            }
        }
        private void datos(string texto)
        {
            dtTriplucion.Rows.Clear();
            foreach (Tripulacion a in new TripulanteBO().Consulta4(texto))
            {
                dtTriplucion.Rows.Add(a, a.cedula, a.nombre, a.nacimiento.ToString("dd/MM/yyyy"), a.aerolinea, a.rol, a.estado);
            }
        }

        private void DlgAdmiCon4_Load(object sender, EventArgs e)
        {
            aerolinea();
        }

        private void cbAerolinea_SelectedIndexChanged(object sender, EventArgs e)
        {
            string texto = cbAerolinea.Text;
            datos(texto);

        }

        private void DlgAdmiCon4_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (parent != null)
            {
                parent.Show();
            }
        }
    }
}
