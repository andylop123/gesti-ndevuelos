﻿using GestionDeVuelos.BO;
using GestionDeVuelos.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionDeVuelos.GUI
{
    public partial class DlgListaVuelo : Form
    {
        private Form parent;
        public DlgListaVuelo(Form parent)
        {
            this.parent = parent;
            InitializeComponent();
            aerolinea();
        }


        /// <summary>
        /// Este metodo cargar los vuelos
        /// </summary>
        /// <param name="a">Representa la aerolinea a buscar</param>
        private void CargarVuelos(Aerolinea a)
        {
            dtVuelos.Rows.Clear();
            foreach (Vuelo vuelo in new VueloBO().CargarVuelosAerolinea(a))
            {
                List<Tripulacion> t = new TripulanteBO().CargarTripulacion(vuelo.Tripulacion);
                dtVuelos.Rows.Add(vuelo.Aerolinea, vuelo.Precio, vuelo.FechaSalida, vuelo.FechaLlegada,
                    vuelo.AeropuertoSalida, vuelo.AeropuertoLlegada, vuelo.Avion.Modelo, t[0], t[1], t[2], t[3], t[4]);
            }
        }

        /// <summary>
        /// Este metodo cargar las aerolineas
        /// </summary>
        private void aerolinea()
        {
            foreach (Aerolinea a in new AerolineaBO().ConsultarDatos())
            {
                cbAerolinea.Items.Add(a);
            }
        }

        private void DlgListaVuelo_FormClosed(object sender, FormClosedEventArgs e)
        {
            if(parent != null)
            {
                parent.Show();
                this.Hide();
            }
        }

        private void cbAerolinea_SelectedIndexChanged(object sender, EventArgs e)
        {
            Aerolinea a = (Aerolinea)cbAerolinea.SelectedItem;

            CargarVuelos(a);
        }

        private void DlgListaVuelo_Load(object sender, EventArgs e)
        {
            dtVuelos.ForeColor = Color.Black;
        }
    }
}
