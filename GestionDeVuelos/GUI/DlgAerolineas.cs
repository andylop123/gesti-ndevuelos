﻿using GestionDeVuelos.BO;
using GestionDeVuelos.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionDeVuelos.GUI
{
    public partial class DlgAerolineas : Form
    {
        private Form parent;
        public DlgAerolineas(Form parent)
        {
            InitializeComponent();
            this.parent = parent;
        }

        public void cargarDatos()
        {
            dtAerolinea.Rows.Clear();
            foreach (Aerolinea a in new AerolineaBO().ConsultarDatos())
            {
                dtAerolinea.Rows.Add(a,a.Id,a.nombre,a.tipo,a.año);
            }
        }

        private void DlgAerolineas_Load(object sender, EventArgs e)
        {
            dtAerolinea.ForeColor = Color.Black;

            cargarDatos();
        }

        private void DlgAerolineas_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (parent != null)
            {
                parent.Show();
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            try
            {

                Aerolinea a = new Aerolinea()
                {
                    nombre = textNombre.Text.Trim(),
                    año = dateAño.Value.Year.ToString(),
                    tipo = cbTipo.Text.Trim()

                };
                if (new AerolineaBO().insertar(a))
                {
                    textNombre.Clear();
                    dateAño.ResetText();
                    cbTipo.Text="";
                    cargarDatos();
                    MessageBox.Show("Aerolinea insertada con exito");

                }

            }
            catch (Exception)
            {

                throw;
            }

        }

        private void textNombre_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
