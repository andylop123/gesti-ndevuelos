﻿using GestionDeVuelos.BO;
using GestionDeVuelos.Entidades;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace GestionDeVuelos.GUI
{
    public partial class DlgAviones : Form
    {
        private Form parent;
        public DlgAviones(Form parent)
        {
            InitializeComponent();
            this.parent = parent;
        }


        private void aerolinea()
        {
            foreach (Aerolinea a in new AerolineaBO().ConsultarDatos())
            {
                cbAerolinea.Items.Add(a);
            }
        }
        private void datos()
        {
            dtTriplucion.Rows.Clear();
            foreach (Tripulacion a in new TripulanteBO().ConsultarDatos())
            {
                dtTriplucion.Rows.Add( a.Id, a.cedula, a.nombre, a.nacimiento.ToString("dd/MM/yyyy"), a.aerolinea, a.rol, a.estado);
            }
        }
        private void DlgAviones_Load(object sender, EventArgs e)
        {
            dtTriplucion.ForeColor = Color.Black;
            aerolinea();
            datos();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Tripulacion t = new Tripulacion
            {
                nombre = textNombre.Text.Trim(),
                cedula = textCedula.Text.Trim(),
                nacimiento = dtFecha.Value,
                aerolinea = (Aerolinea)cbAerolinea.SelectedItem,
                rol = cbRol.Text.Trim()
            };
            if (new TripulanteBO().insertar(t))
            {
                MessageBox.Show("Tripulación insertada correctamente");
                datos();
                textNombre.Clear();
                textCedula.Clear();
                dtFecha.ResetText();
                cbAerolinea.Text = "";
                cbRol.Text = "";





            }
        }

        private void DlgAviones_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (parent != null) {
                parent.Show();
            }
        }
    }
}
