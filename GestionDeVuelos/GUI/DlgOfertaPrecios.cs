﻿using GestionDeVuelos.BO;
using GestionDeVuelos.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionDeVuelos.GUI
{
    public partial class DlgOfertaPrecios : Form
    {
        private Form parent;
        private Vuelo vuelo;

        public DlgOfertaPrecios(Form parent, Vuelo vuelo)
        {
            InitializeComponent();
            this.parent = parent;
            this.vuelo = vuelo;
        }

        private void DlgOfertaPrecios_Load(object sender, EventArgs e)
        {
            if (vuelo != null)
            {

                CargarVuelos(vuelo);

            }
        }
        /// <summary>
        /// Este metodo cargar los datos de los vuelos
        /// </summary>
        /// <param name="vuelo">Presenta los vuelos a buscar</param>
        private void CargarVuelos(Vuelo vuelo)
        {

            List<string> vuelos = new VueloBO().CargarVuelosFechas(vuelo);
            foreach (string v in vuelos)
            {
                string[] datos = v.Split('-');
                if (datos.Length == 3)
                {
                    dataVuelos.Rows.Add(datos[0], datos[2], datos[1]);

                }
                else if (datos.Length == 4)
                {
                    dataVuelos.Rows.Add(datos[0] + "," + datos[1], datos[3], datos[2]);
                }
                else if (datos.Length >= 5) {

                    dataVuelos.Rows.Add(datos[0] + "," + datos[1] + "," + datos[2],datos[3],datos[4]);
                }
            }
        }



        private void DlgOfertaPrecios_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (parent != null)
            {
                parent.Show();
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
