﻿using GestionDeVuelos.BO;
using GestionDeVuelos.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionDeVuelos.GUI
{
    public partial class FrmRegistroUsuarios : Form
    {
        private Form parent;
        public FrmRegistroUsuarios(Form parent)
        {
            InitializeComponent();
            this.parent = parent;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string ced = txtCed.Text.Trim();
            string nom = txtNom.Text.Trim();
            string contra = txtContra.Text.Trim();
            int edad = Int32.Parse(txtEdad.Text.Trim());
            Usuario usu = new Usuario
            {
                Cedula = ced,
                Nombre = nom,
                Contrasena = contra,
                Edad = edad
            };
            if (new UsuarioBO().Insertar(usu)) {
                MessageBox.Show("Registro realizado con exito");
                Limpiar();
            }
        }

        private void Limpiar() {
            txtCed.Clear();
            txtNom.Clear();
            txtEdad.Clear();
            txtContra.Clear();
        
        }

        private void FrmRegistroAdministrador_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (parent != null) {
                parent.Show();
            }
        }

        private void FrmRegistroUsuarios_Load(object sender, EventArgs e)
        {

        }
    }

        
            
    
}
