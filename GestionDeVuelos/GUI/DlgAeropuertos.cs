﻿using GestionDeVuelos.BO;
using GestionDeVuelos.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionDeVuelos.GUI
{
    public partial class DlgAeropuertos : Form
    {
        private Form parent;
        public DlgAeropuertos(Form parent)
        {
            InitializeComponent();
            this.parent = parent;
            CargarDatos("");
        }

        private void CargarDatos(string filtro) {

            dataGridView1.Rows.Clear();
            foreach (Aeropuerto a in new AeropuertoBO().Seleccionar(filtro))
            {
                dataGridView1.Rows.Add(a.Iata, a.Nombre, a.Pais);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string iata = txtIata.Text.Trim();
            string nombre = txtNombre.Text.Trim();
            string pais = txtPais.Text.Trim();
            Aeropuerto a = new Aeropuerto { Iata = iata, Nombre = nombre, Pais = pais };
            if (new AeropuertoBO().Insertar(a)) {
                txtIata.Clear();
                txtNombre.Clear();
                txtPais.Clear();
                MessageBox.Show("Registro realizado con éxito");
            }
            CargarDatos("");

        }

        private void DlgAeropuertos_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (parent != null) {
                parent.Show();
            }
        }

        private void DlgAeropuertos_Load(object sender, EventArgs e)
        {

        }
    }
}
