﻿using GestionDeVuelos.BO;
using GestionDeVuelos.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace GestionDeVuelos.GUI
{
    public partial class DlgRankingAviones : Form
    {
        private Form parent;
        public DlgRankingAviones(Form parent)
        {
            InitializeComponent();
            this.parent = parent;
            CargarAerolineas();

        }
        /// <summary>
        /// Este metodo carga las aerolineas que estan en la base de datos
        /// </summary>
        private void CargarAerolineas()
        {
            foreach (Aerolinea a in new AerolineaBO().ConsultarDatos())
            {
                cbAerolineas.Items.Add(a);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(cbAerolineas.Text)) {
                Aerolinea a = (Aerolinea)cbAerolineas.SelectedItem;
                chart1.Series.Clear();
                chart1.Series.Add("Aviones");
                List<ReporteRankingVuelos> ranking = new ReporteRankingVuelosBO().Seleccionar(a);
 
                foreach (ReporteRankingVuelos r in ranking)
                {

                    Series serie = chart1.Series.Add(r.Avion.ToString());
                    serie.Label += r.Avion.Modelo;
                    serie.Points.Add(r.Cantidad);
                }
            }
        }

        private void DlgRankingAviones_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (parent != null) {
                parent.Show();
            }
        }

        private void DlgRankingAviones_Load(object sender, EventArgs e)
        {

        }
    }
}
