﻿using GestionDeVuelos.BO;
using GestionDeVuelos.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionDeVuelos.GUI
{
    public partial class DlgOfertaVuelos : Form
    {
        private Form parent;
        private string txt = " ";
        private Usuario pasajero;
        private bool compra;
        public DlgOfertaVuelos(Form parent, Usuario pasa)
        {
            InitializeComponent();
            this.parent = parent;
            this.pasajero = pasa;
            this.compra = true;
        }


        /// <summary>
        /// Este metodo crea una matriz de botones
        /// </summary>
        /// <param name="vuelos">Representa los vuelos a colcar en la matriz</param>
        private void MatrizVuelos(List<string> vuelos)
        {
            Button[,] boton = new Button[vuelos.Count, 1];
            panel5.Controls.Clear();

            for (int i = 0; i < vuelos.Count; i++)
            {
                for (int j = 0; j < 1; j++)
                {

                    boton[i, j] = new Button();
                    boton[i, j].Width = panel5.Width;
                    //MessageBox.Show(vuelos.ElementAt(i) +vuelos.ElementAt(i).Length);
                    if (vuelos.ElementAt(i).Length <= 50)
                    {
                        boton[i, j].Height = 100;
                        boton[i, j].Top = i * 100;

                    }
                    else if (vuelos.ElementAt(i).Length <= 120)
                    {
                        boton[i, j].Height = 250;
                        boton[i, j].Top = i * 250;

                    }
                    else if (vuelos.ElementAt(i).Length <= 160)
                    {
                        boton[i, j].Height = 350;
                        boton[i, j].Top = i * 350;

                    }
                    else
                    {
                        boton[i, j].Height = 450;
                        boton[i, j].Top = i * 450;
                    }

                    boton[i, j].Text = vuelos.ElementAt(i);
                    boton[i, j].BackColor = Color.FromArgb(20, 83, 114);
                    boton[i, j].ForeColor = Color.White;
                    boton[i, j].Font = new Font(panel5.Font, FontStyle.Bold);
                    boton[i, j].TextAlign = ContentAlignment.MiddleCenter;
                    //boton[i, j].Click += new EventHandler(GreetingBtn_Click);
                    boton[i, j].Click += new EventHandler(BtnClick);
                    panel5.Controls.Add(boton[i, j]);
                    panel5.Update();

                }

            }
        }

        /// <summary>
        /// Este metodo crea una matriz de botones
        /// </summary>
        /// <param name="vuelos">Representa los vuelos a colocar dentro de la matriz</param>
        /// <param name="f">Representa el formato a dar</param>
        /// <param name="p">Representa el panel donde colcoar la matriz</param>
        private void CrearMatriz(List<string> vuelos, int f, Panel p)
        {
            Button[,] boton = new Button[vuelos.Count, 1];
            p.Controls.Clear();
            for (int i = 0; i < vuelos.Count; i++)
            {
                for (int j = 0; j < 1; j++)
                {
                    boton[i, j] = new Button();
                    boton[i, j].Width = panel5.Width;
                    boton[i, j].Height = 100;
                    boton[i, j].TextAlign = ContentAlignment.MiddleCenter;
                    boton[i, j].Text = vuelos.ElementAt(i);
                    boton[i, j].Top = i * 100;
                    boton[i, j].Left = j * 100;
                    boton[i, j].BackColor = Color.FromArgb(20, 83, 114);
                    boton[i, j].ForeColor = Color.White;
                    boton[i, j].Font = new Font(panel5.Font, FontStyle.Bold);
                    boton[i, j].Click += new EventHandler(GreetingBtn_Click);
                    p.Controls.Add(boton[i, j]);
                    p.Update();

                }

            }
        }


        /// <summary>
        /// Este metodo carga una lista de vuelos
        /// </summary>
        /// <param name="v">Representa los vuelos a buscar</param>
        /// <returns>Devuelve una lista de vuelos</returns>
        private List<string> lista(Vuelo v)
        {
            List<string> vuelos5 = new List<string>();
            if (cbxBusqueda.Checked)
            {
                int r = Int32.Parse(cbRango.Text);
                List<string> vuelos = new VueloBO().VuelosInteligentes(v, r, 1);
                foreach (string item in vuelos)
                {
                    vuelos5.Add(item);

                    if (vuelos5.Count == 5)
                    {

                        return vuelos5;

                    }
                }

            }
            return null;


        }

        /// <summary>
        /// Este metodo carga los vuelos dentro de la matriz
        /// </summary>
        private void CargarData()
        {
            compra = false;
            List<string> vuelos = new VueloBO().Seleccionar();
            if (vuelos != null)
            {

                MatrizVuelos(vuelos);

            }
            else
            {
                LimpiarPanel(panel5);
            }

        }

        /// <summary>
        /// Este metodo limpia la matriz
        /// </summary>
        /// <param name="panel">Representa el panel limpiar</param>
        private void LimpiarPanel(Panel panel)
        {
            panel.Controls.Clear();
        }

  

        private void BtnClick(Object sender, EventArgs e)
        {
            if (compra && rbIda.Checked)
            {
                Button b = (Button)sender;
                string[] lineas = b.Text.Split('\n');
                List<int> ids = new List<int>();
                for (int i = 0; i < lineas.Length; i++)
                {
                    int id = 0;
                    if (Int32.TryParse(lineas[i], out id))
                    {
                        ids.Add(id);

                    }
                }
                if (ids != null)
                {
                    if (MessageBox.Show("¿Seguro que desea comprar el vuelo?", "Compra de vuelo", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {

                        FrmPagoVuelo frm = new FrmPagoVuelo(this, pasajero, ids);
                        frm.Show();
                        this.Hide();

                    }

                }
            }

        }
        private void GreetingBtn_Click(Object sender, EventArgs e)
        {

            Button clickedButton = (Button)sender;
            string[] ids = clickedButton.Text.Split('\n');



            if (cbxBusqueda.Checked == false)
            {


                if (MessageBox.Show("¿Desea comprar el vuelo ?", "Pagar vuelo", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    VueloBO ubo = new VueloBO();
                    if (clickedButton.Text.Length >= 45 && clickedButton.Text.Length <= 90)
                    {
                        string s = ids[0];
                        int id;
                        if (int.TryParse(s, out id))
                        {
                            Vuelo ida = ubo.SeleccionarID(id);
                            if (ida != null)
                            {
                                Historial h = new Historial()
                                {
                                    cedula = pasajero.Cedula,
                                    paisSalida = ida.AeropuertoSalida.Pais,
                                    paisLlegada = ida.AeropuertoLlegada.Pais,
                                    paisEscala1 = "Sin primera escala",
                                    paisEscala2 = "Sin segunda escala",
                                    fechaHora = DateTime.Now,
                                    costo = ida.Precio,
                                    duracion = (ida.Duracion) / 60
                                };



                                if (new HistorialBO().Insertar(h))
                                {

                                    MessageBox.Show("Vuelo comprado con éxito");
                                }


                            }

                        }
                    }
                    else if (clickedButton.Text.Length >= 95 && clickedButton.Text.Length <= 125)
                    {
                        string s = ids[0];
                        int id;

                        string s2 = ids[4];
                        int id2;
                        if (int.TryParse(s, out id) && int.TryParse(s2, out id2) || int.TryParse(ids[5], out id2))
                        {


                            Vuelo ida = ubo.SeleccionarID(id);
                            if (ida != null)
                            {

                                Vuelo ida2 = ubo.SeleccionarID(id2);
                                if (ida2 != null)
                                {
                                    Historial h = new Historial()
                                    {
                                        cedula = pasajero.Cedula,
                                        paisSalida = ida.AeropuertoSalida.Pais,
                                        paisLlegada = ida2.AeropuertoLlegada.Pais,
                                        paisEscala1 = ida.AeropuertoLlegada.Pais,
                                        paisEscala2 = "Sin segunda escala",
                                        fechaHora = DateTime.Now,
                                        costo = ida.Precio + ida2.Precio,
                                        duracion = (ida.Duracion + ida2.Duracion) / 60
                                    };

                                    Console.WriteLine("Hola 2");
                                    if (new HistorialBO().Insertar(h))
                                    {

                                        MessageBox.Show("Vuelo comprado con éxito");
                                    }
                                }

                            }




                        }


                    }
                    else if (clickedButton.Text.Length >= 140 || clickedButton.Width == 400)
                    {
                        string s = ids[0];
                        int id;

                        string s2 = ids[4];
                        int id2;

                        string s3 = ids[8];
                        int id3;


                        if (int.TryParse(s, out id) && (int.TryParse(s2, out id2) || int.TryParse(ids[5], out id2)) && (int.TryParse(s3, out id3) || int.TryParse(ids[10], out id3)))
                        {


                            Vuelo ida = ubo.SeleccionarID(id);
                            if (ida != null)
                            {

                                Vuelo ida2 = ubo.SeleccionarID(id2);
                                if (ida2 != null)
                                {

                                    Vuelo ida3 = ubo.SeleccionarID(id3);

                                    if (ida3 != null)
                                    {
                                        Historial h = new Historial()
                                        {
                                            cedula = pasajero.Cedula,
                                            paisSalida = ida.AeropuertoSalida.Pais,
                                            paisLlegada = ida3.AeropuertoLlegada.Pais,
                                            paisEscala1 = ida2.AeropuertoSalida.Pais,
                                            paisEscala2 = ida2.AeropuertoLlegada.Pais,
                                            fechaHora = DateTime.Now,
                                            costo = ida.Precio + ida2.Precio + ida3.Precio,
                                            duracion = (ida.Duracion + ida2.Duracion + ida3.Duracion) / 60
                                        };
                                        if (new HistorialBO().Insertar(h))
                                        {

                                            MessageBox.Show("Vuelo comprado con éxito");
                                        }




                                    }


                                }

                            }




                        }






                    }


                }


                else

                {
                    return;

                }
            }


        }



        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Aeropuerto origen = new Aeropuerto { Nombre = txtOrigen.Text.Trim() };
            Aeropuerto destino = new Aeropuerto { Nombre = txtDestino.Text.Trim() };
            DateTime fechaSalida = dtSalida.Value;
            DateTime fechaLlegada = dtLlegada.Value;
            Vuelo v = new Vuelo
            {
                AeropuertoSalida = origen,
                AeropuertoLlegada = destino,
                FechaSalida = fechaSalida,
                FechaLlegada = fechaLlegada
            };
            if (rbIda.Checked)
            {

                if (cbxBusqueda.Checked)
                {
                    int r = Int32.Parse(cbRango.Text);
                    List<string> vuelos = lista(v);

                    if (vuelos != null)
                    {

                        MatrizVuelos(vuelos);
                    }
                    else
                    {
                        panel5.Controls.Clear();

                    }
                }
                else
                {
                    compra = true;
                    List<string> vuelos = new VueloBO().VuelosIda(v, 1);

                    if (vuelos != null)
                    {
                        MatrizVuelos(vuelos);
                    }
                    else
                    {
                        LimpiarPanel(panel5);

                    }



                }
            }
            else if (rbIdaVuelta.Checked)
            {

                if (cbxBusqueda.Checked == true)
                {

                    int r = Int32.Parse(cbRango.Text);
                    List<string> vuelos = new VueloBO().VuelosVueltaInteligente(v, r);
                    if (vuelos != null)
                    {
                        MatrizVuelos(vuelos);
                    }
                    else
                    {
                        panel5.Controls.Clear();

                    }
                }
                else
                {

                    List<string> vuelos = new VueloBO().VuelosVuelta(v);
                    if (vuelos != null)
                    {
                        MatrizVuelos(vuelos);
                    }
                    else
                    {
                        panel5.Controls.Clear();
                    }


                }


            }



        }

        private void cbxBusqueda_CheckedChanged(object sender, EventArgs e)
        {
            if (cbxBusqueda.Checked == true)
            {
                Rango.Visible = true;
                cbRango.Visible = true;

            }
            else
            {

                Rango.Visible = false;
                cbRango.Visible = false;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //CargarPrecio();
            //panel5.Update();
            compra = false;
            Aeropuerto origen = new Aeropuerto { Nombre = txtOrigen.Text.Trim() };
            Aeropuerto destino = new Aeropuerto { Nombre = txtDestino.Text.Trim() };
            DateTime fechaSalida = dtSalida.Value;
            DateTime fechaLlegada = dtLlegada.Value;
            Vuelo v = new Vuelo
            {
                AeropuertoSalida = origen,
                AeropuertoLlegada = destino,
                FechaSalida = fechaSalida,
                FechaLlegada = fechaLlegada
            };

            if (rbIda.Checked)
            {

                List<string> vuelos = new VueloBO().VuelosIda(v, 2);
                if (vuelos != null)
                {
                    MatrizVuelos(vuelos);
                }
                else
                {
                    LimpiarPanel(panel5);

                }


            }






        }

        private void DlgOfertaVuelos_Load(object sender, EventArgs e)
        {
            CargarData();
        }

        private void txtOrigen_KeyUp(object sender, KeyEventArgs e)
        {
            string filtro = txtOrigen.Text.Trim();
            if (filtro.Length >= 3)
            {
                Aeropuerto a = CargarAeropuerto(filtro);
                if (a != null)
                {
                    txtOrigen.Text = a.Nombre;
                }
                else
                {
                    txtOrigen.Clear();

                }
            }

        }

        /// <summary>
        /// Este metodo cargar un aeropuerto
        /// </summary>
        /// <param name="filtro">Representa el filtro por el que buscar el aeropuerto</param>
        /// <returns>Devuelve un aeropuerto</returns>
        private Aeropuerto CargarAeropuerto(string filtro)
        {
            return new AeropuertoBO().CargarAeropuerto(filtro);

        }

        private void txtDestino_KeyUp(object sender, KeyEventArgs e)
        {
            string filtro = txtDestino.Text.Trim();
            if (filtro.Length > 2)
            {
                Aeropuerto a = CargarAeropuerto(filtro);
                if (a != null)
                {
                    txtDestino.Text = a.Nombre;
                }
                else
                {
                    txtDestino.Clear();

                }
            }
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            Aeropuerto aeroSal = new Aeropuerto { Nombre = txtOrigen.Text.Trim() };
            Aeropuerto aeroLleg = new Aeropuerto { Nombre = txtDestino.Text.Trim() };
            DateTime fecha = dtSalida.Value;
            Vuelo v = new Vuelo { AeropuertoSalida = aeroSal, AeropuertoLlegada = aeroLleg, FechaSalida = fecha };
            DlgOfertaPrecios dlg = new DlgOfertaPrecios(this, v);
            dlg.Show();
            this.Hide();
        }

        private void DlgOfertaVuelos_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (parent != null)
            {
                parent.Show();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            compra = false;
            Aeropuerto origen = new Aeropuerto { Nombre = txtOrigen.Text.Trim() };
            Aeropuerto destino = new Aeropuerto { Nombre = txtDestino.Text.Trim() };
            DateTime fechaSalida = dtSalida.Value;
            DateTime fechaLlegada = dtLlegada.Value;
            Vuelo v = new Vuelo
            {
                AeropuertoSalida = origen,
                AeropuertoLlegada = destino,
                FechaSalida = fechaSalida,
                FechaLlegada = fechaLlegada
            };

            if (rbIda.Checked)
            {

                List<string> vuelos = new VueloBO().VuelosIda(v, 3);
                if (vuelos != null)
                {
                    MatrizVuelos(vuelos);
                }
                else
                {
                    LimpiarPanel(panel5);

                }


            }
        }
    }
}
