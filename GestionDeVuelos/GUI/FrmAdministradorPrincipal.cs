﻿using GestionDeVuelos.BO;
using GestionDeVuelos.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionDeVuelos.GUI
{
    public partial class FrmAdministradorPrincipal : Form
    {
        private Form parent;
        public FrmAdministradorPrincipal(Form parent)
        {
            InitializeComponent();
            this.parent = parent;
        }


        private void aerolineasToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void aerolineasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Hoaa");
        }

        private void label1_Click(object sender, EventArgs e)
        {
            panel1.Visible = true;
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void FrmAdministradorPrincipal_Load(object sender, EventArgs e)
        {
       

        }

        private void FrmAdministradorPrincipal_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (parent != null)
            {
                parent.Show();
            }
        }


        private void aerolineasToolStripMenuItem1_Click_1(object sender, EventArgs e)
        {
            DlgAerolineas dlg = new DlgAerolineas(this);
            dlg.Show();
            this.Hide();
        }

  
        private void tripulacionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DlgAviones dlg = new DlgAviones(this);
            dlg.Show();
            this.Hide();

        }

        private void avionesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            DlgAvioes dlg = new DlgAvioes(this);
            this.Hide();
            dlg.Show();
        }

        private void aeropuertosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            DlgAeropuertos dlg = new DlgAeropuertos(this);
            this.Hide();
            dlg.Show();
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void vuelosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            DlgVuelos dlg = new DlgVuelos(this);
            this.Hide();
            dlg.Show();
        }

        private void toolStripMenuItem1_Click_1(object sender, EventArgs e)
        {
            DlgListaVuelo dlg = new DlgListaVuelo(this);
            this.Hide();
            dlg.Show();
        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
                Vuelo v = new Vuelo();
                v.FechaSalida = dtSalida.Value;
                v.FechaLlegada = dtLlegada.Value;
                CargarVuelosRango(v);
      
        }

        /// <summary>
        /// Este metodo cargar los vuelos dentro de un rango de fechas
        /// </summary>
        /// <param name="v">Representa los vuelos a buscar</param>
        private void CargarVuelosRango(Vuelo v)
        {
            dataVuelos.Rows.Clear();
            foreach (Vuelo vuelo in new VueloBO().CargarVuelosRango(v))
            {
                List<Tripulacion> t = new TripulanteBO().CargarTripulacion(vuelo.Tripulacion);
                dataVuelos.Rows.Add(vuelo.Aerolinea,vuelo.Precio,vuelo.FechaSalida,vuelo.FechaLlegada,
                    vuelo.AeropuertoSalida,vuelo.AeropuertoLlegada,vuelo.Avion.Modelo,t[0],t[1],t[2],t[3],t[4]);
            }
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }

        private void miembrosDeUnaTripulaciónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DlgAdmiCon4 dlg = new DlgAdmiCon4(this);
            dlg.Show();
            this.Hide();
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            DlgRankingAviones dlg = new DlgRankingAviones(this);
            this.Hide();
            dlg.Show();
        }
    }
}
