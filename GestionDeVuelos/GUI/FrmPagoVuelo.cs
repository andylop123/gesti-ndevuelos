﻿using GestionDeVuelos.BO;
using GestionDeVuelos.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionDeVuelos.GUI
{
    public partial class FrmPagoVuelo : Form
    {
        private Form parent;
        private Usuario u;
        private List<int> vuelo;
        public FrmPagoVuelo(Form parent, Usuario u, List<int> vuelo)
        {
            InitializeComponent();
            this.parent = parent;
            this.u = u;
            this.vuelo = vuelo;
        }

        private void FrmPagoVuelo_Load(object sender, EventArgs e)
        {
            CargarVuelo();
        }

        /// <summary>
        /// Este metodo cargar los vuelos posibles
        /// </summary>
        private void CargarVuelo()
        {
            List<Vuelo> vuelos = new List<Vuelo>();
            for (int i = 0; i < vuelo.Count; i++)
            {
                Vuelo v = new Vuelo { Id = vuelo[i] };
                v = new VueloBO().CargarId(v);
                if (v != null)
                {
                    vuelos.Add(v);
                }
            }
            CargarDatos(vuelos);
        }


        /// <summary>
        /// Este metodo carga los datos de los posibles vuelos
        /// </summary>
        /// <param name="v">Representa la lista de vuelos a comprar</param>
        private void CargarDatos(List<Vuelo> v)
        {
            List<Vuelo> vuelos = v;
            if (vuelos != null)
            {
                int cantidad = vuelos.Count;
                txtFecha.Text = DateTime.Now.ToString();
                txtCed.Text = u.Cedula;
                txtPaisSal.Text = vuelos[0].AeropuertoSalida.Pais;
                txtPaisLleg.Text = vuelos[cantidad - 1].AeropuertoLlegada.Pais;
                int duracion = vuelos[0].Duracion;
                int precio = vuelos[0].Precio;
                if (cantidad >= 2)
                {
                    lbEscalaUno.Visible = true;
                    txtEscalaUno.Visible = true;
                    txtEscalaUno.Text = vuelos[1].AeropuertoSalida.Pais;
                    duracion += vuelos[1].Duracion;
                    precio += vuelos[1].Precio;
                }
                if (cantidad == 3)
                {
                    lbEscalaDos.Visible = true;
                    txtEscalaDos.Visible = true;
                    txtEscalaDos.Text = vuelos[2].AeropuertoSalida.Pais;
                    duracion += vuelos[2].Duracion;
                    precio += vuelos[2].Precio;
                }
                txtDurac.Text = (duracion / 60).ToString();
                txtCosto.Text = precio.ToString();
            }

        }

        private void FrmPagoVuelo_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (parent != null)
            {
                parent.Show();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string ced = txtCed.Text.Trim();
            string paisSalida = txtPaisSal.Text.Trim();
            string paisLleg = txtPaisLleg.Text.Trim();
            DateTime fecha = Convert.ToDateTime(txtFecha.Text);
            string primerEscala = string.IsNullOrEmpty(txtEscalaUno.Text) ? "Sin primer escala  " : txtEscalaUno.Text ;
            string segundaEscala = string.IsNullOrEmpty(txtEscalaDos.Text) ? "Sin segunda escala " : txtEscalaDos.Text;
            string precio = txtCosto.Text.Trim();
            string duracion = txtDurac.Text.Trim();
            Historial h = new Historial
            {
                cedula = ced,
                paisSalida = paisSalida,
                paisLlegada = paisLleg,
                fechaHora = fecha,
                paisEscala1 = primerEscala,
                paisEscala2 = segundaEscala,
                costo = Int32.Parse(precio),
                duracion = Int32.Parse(duracion)
            };
            if (new HistorialBO().Insertar(h))
            {
                MessageBox.Show("Vuelo comprado con éxito");
                Limpiar();
                this.Close();
            }

        }

        /// <summary>
        /// Este metodo limpia los datos de la interfaz
        /// </summary>
        private void Limpiar()
        {
            txtCed.Clear();
            txtPaisSal.Clear();
            txtPaisLleg.Clear();
            txtFecha.Clear();
            txtEscalaUno.Clear();
            txtEscalaDos.Clear();
            txtCosto.Clear();
            txtDurac.Clear();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (parent != null)
            {
                parent.Show();
                this.Close();
            }
        }
    }
}
