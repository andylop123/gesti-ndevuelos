﻿using GestionDeVuelos.BO;
using GestionDeVuelos.Entidades;
using System;
using System.Windows.Forms;

namespace GestionDeVuelos.GUI
{
    public partial class DlgAvioes : Form
    {
        private Form parent;
        public DlgAvioes(Form parent)
        {
            InitializeComponent();
            this.parent = parent;
            CargarDatos("");
            CargarAerolinea();

        }

        /// <summary>
        /// Este metodo cargar los datos de las Aerolineas
        /// </summary>
        private void CargarAerolinea()
        {

            foreach (Aerolinea a in new AerolineaBO().ConsultarDatos())
            {
                cbAerolineas.Items.Add(a);
            }
        }

        /// <summary>
        /// Este metodo cargar los datos de los aviones
        /// </summary>
        /// <param name="filtro">Representa el filtro de busqueda</param>
        private void CargarDatos(string filtro)
        {
            dataGridView1.Rows.Clear();
            foreach (Avion a in new AvionBO().Seleccionar(filtro))
            {
                dataGridView1.Rows.Add(a.Modelo, a.Aerolinea.nombre, a.Estado, a.Capacidad, a.Ano);
            }

        }

        private void DlgAvioes_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (parent != null)
            {
                parent.Show();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string modelo = txtModelo.Text.Trim();
            string ano = dateAño.Value.Year.ToString();
            Aerolinea ae = (Aerolinea)cbAerolineas.SelectedItem;
            string capacidad = txtCapacidad.Text.Trim();
            Avion a = new Avion { Modelo = modelo, Ano = ano, Aerolinea = ae, Capacidad = capacidad };
            if (new AvionBO().Insertar(a))
            {
                txtModelo.Clear();
                dateAño.ResetText();
                txtModelo.Clear();
                txtCapacidad.Clear();
                cbAerolineas.Text = "";
                MessageBox.Show("Registro realizado con éxito");
                CargarDatos("");
            }
        }

        private void DlgAvioes_Load(object sender, EventArgs e)
        {

        }
    }
}
