﻿using GestionDeVuelos.DAO;
using GestionDeVuelos.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionDeVuelos.BO
{
    class AvionBO
    {
        /// <summary>
        /// Inserta un Avion
        /// </summary>
        /// <param name="a"> Es un Avion</param>
        /// <returns>Inserta un Avion en la base de datos</returns>
        public bool Insertar(Avion a) {
            return new AvionDAO().Insertar(a);
        }

        /// <summary>
        /// Carga un Avion por un filto
        /// </summary>
        /// <param name="filtro"> Filtro por el cual se va cargar el Avion</param>
        /// <returns>Devuelve una lista de Aviones</returns>
        public List<Avion> Seleccionar(string filtro) {
            return new AvionDAO().Seleccionar(filtro);
        }

        /// <summary>
        /// Carga una lista de modelos de aviones
        /// </summary>
        /// <returns> devuelve la lista de aviones</returns>
        public List<string> Modelos() {
            return new AvionDAO().Modelos();
        }
    }
}
