﻿using GestionDeVuelos.DAO;
using GestionDeVuelos.Entidades;
using GestionDeVuelos.GUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionDeVuelos.BO
{
    class VueloBO
    {




        /// <summary>
        /// Este metodo devuelve un vuelo segun su Id
        /// </summary>
        /// <param name="v">Representa el vuelo a buscar</param>
        /// <returns>Devuelve un vuelo</returns>
        public Vuelo CargarId(Vuelo v)
        {
            return new VueloDAO().CargarId(v);
        }

        /// <summary>
        /// Este metodo inserta un vuelo en la base de datos
        /// </summary>
        /// <param name="v">Representa el vuelo a insertar</param>
        /// <returns>Devuelve true si se lorgro insertar</returns>
        public bool Insertar(Vuelo v)
        {

            v.Tripulacion = SeleccionarTripulacion(v);
            v.Avion = SelccionarAvio(v);

            return new VueloDAO().Insertar(v);

        }

        private Avion SelccionarAvio(Vuelo v)
        {
            Avion a = new AvionDAO().CargarAerolinea(v);
            if (a == null)
            {
                throw new Exception("No hay aviones disponibles");
            }


            return a;
        }

        /// <summary>
        /// Este metodo devuelve una lista de vuelos
        /// </summary>
        /// <returns>Devuelve una lista de vuelos</returns>
        public List<string> Seleccionar()
        {
            List<string> vuel = new List<string>();
            List<Vuelo> vuelos = new VueloDAO().Seleccionar();

            if (vuelos != null)
            {
                foreach (Vuelo v in vuelos)
                {
                    string text = "";
                    text += FormatoSimple(v, 1);
                    vuel.Add(text);
                }
                return vuel;
            }
            return null;
        }

        /// <summary>
        /// Este metodo devuelve una lista de vuelos
        /// </summary>
        /// <param name="v">Representa los vuelos a buscar</param>
        /// <returns>Devuelve una lista de vuelos</returns>
        public List<string> CargarVuelosFechas(Vuelo v)
        {
            List<DateTime> fechas = ListaFecha(v.FechaSalida);
            List<string> vuelos = new List<string>();
            for (int i = 0; i < fechas.Count; i++)
            {
                v.FechaSalida = fechas[i];
                List<string> lista = VuelosIda(v, 1);
                foreach (string s in lista)
                {
                    vuelos.Add(s);
                }

            }
            return DatosVuelos(vuelos);
        }

        /// <summary>
        /// Este metodo devuelve una lista de strings
        /// </summary>
        /// <param name="v">Este metodo es una lista de vuelos pero en formato string</param>
        /// <returns>Devuelve una lista de string</returns>
        public List<string> DatosVuelos(List<string> v)
        {
            List<string> vuelos = new List<string>();
            foreach (string vuelo in v)
            {
                string[] lineas = vuelo.Split('\n');
                List<int> ids = new List<int>();
                List<Vuelo> vuel = new List<Vuelo>();
                for (int i = 0; i < lineas.Length; i++)
                {
                    int id = Int32.TryParse(lineas[i], out id) == true ? Int32.Parse(lineas[i]) : -1;
                    if (id != -1)
                    {
                        ids.Add(id);
                    }
                }
                foreach (int i in ids)
                {
                    Vuelo vue = new Vuelo { Id = i };
                    vue = new VueloDAO().CargarId(vue);
                    if (vue != null)
                    {
                        vuel.Add(vue);
                    }

                }
                vuelos.Add(CargarDatosVuelos(vuel));

            }

            return vuelos;

        }

        /// <summary>
        /// Este metodo devuelve los datos de un vuelo en formato string
        /// </summary>
        /// <param name="vuelos">Representa la lista de vuelos que quiero obetener los datos</param>
        /// <returns>Devuelve un string con formato</returns>
        public string CargarDatosVuelos(List<Vuelo> vuelos)
        {
            string aerolinea = "";
            int precio = 0;
            int duracion = 0;
            foreach (Vuelo v in vuelos)
            {
                aerolinea += v.Aerolinea.nombre + "-";
                precio += v.Precio;
                duracion += v.Duracion;
            }
            return aerolinea + precio + "-" + duracion;
        }

        /// <summary>
        /// Este metodo devuelve una lista de fechas
        /// </summary>
        /// <param name="f">Representa la fecha default</param>
        /// <returns>Devuelve una lista de fechas</returns>
        public List<DateTime> ListaFecha(DateTime f)
        {
            DateTime fecha = f;
            List<DateTime> fechas = new List<DateTime>();
            fechas.Add(fecha.AddDays(-1));
            fechas.Add(f);
            fechas.Add(fecha.AddDays(1));
            return fechas;

        }

        /// <summary>
        /// Este metodo devuelve una lista de vuelos
        /// </summary>
        /// <returns>Devuelve todo los vuelos de la base de datos</returns>
        public List<Vuelo> Cargar()
        {
            return new VueloDAO().Seleccionar();
        }

        /// <summary>
        /// Este metodo selecciona la tripulacion de un vuelo
        /// </summary>
        /// <param name="v">Representa el vuelo</param>
        /// <returns>devuelve un string con los ids de los tripulantes</returns>
        public string SeleccionarTripulacion(Vuelo v)
        {
            string txt = "";
            List<Tripulacion> tripulacion = new TripulanteBO().Seleccionar(v);
            List<Tripulacion> pilotos = new List<Tripulacion>();
            List<Tripulacion> servicio = new List<Tripulacion>();
            foreach (Tripulacion t in tripulacion)
            {
                if (t != null)
                {

                    if (t.rol.Equals("Piloto") && t.estado.Equals("Disponible"))
                    {
                        pilotos.Add(t);
                    }
                    else if (t.rol.Equals("Servicio al cliente") && t.estado.Equals("Disponible"))
                    {
                        servicio.Add(t);
                    }


                }
            }
            if (pilotos.Count >= 2)
            {
                for (int i = 0; i < 2; i++)
                {
                    txt += String.Format(pilotos[i].Id + "-");

                }
            }
            else
            {
                throw new Exception("No hay suficientes pilotos");
            }

            if (tripulacion.Count >= 3)
            {
                for (int i = 0; i < 3; i++)
                {
                    txt += String.Format(servicio[i].Id + "-");
                }
            }
            else
            {
                throw new Exception("No hay tripulación suficiente");
            }

            return txt.Substring(0, txt.Length - 1);

        }

        /// <summary>
        /// Este metodo devuelve una lista de vuelos
        /// </summary>
        /// <param name="v">Representa los datos de los vuelos a buscar</param>
        /// <returns>Devuelve una lista de vuelos</returns>
        public List<Vuelo> CargarVuelosRango(Vuelo v)
        {
            return new VueloDAO().CargarVuelosRango(v);
        }


        /// <summary>
        /// Este metodo devuelve una lista de vuelos segun su aerolinea
        /// </summary>
        /// <param name="v">Representa la aerolinea a buscar</param>
        /// <returns>Devuelve una lista de vuelos</returns>
        public List<Vuelo> CargarVuelosAerolinea(Aerolinea v)
        {
            return new VueloDAO().CargarVuelosAerolinea(v);
        }


        /// <summary>
        /// Este metodo devuelve una lista de vuelos
        /// </summary>
        /// <param name="v">Representa los vuelos a buscar</param>
        /// <returns>Devuelve una lista de string con los vuelos</returns>
        public List<String> VuelosVuelta(Vuelo v)
        {
            List<string> vuel = new List<string>();
            List<string> vuelosIda = new List<string>();
            vuelosIda = VuelosIda(v, 1);

            if (vuelosIda != null)
            {
                List<string> vuelosVuelta = new List<string>();
                Aeropuerto a = v.AeropuertoSalida;
                Vuelo vuelo = new Vuelo
                {
                    AeropuertoSalida = v.AeropuertoLlegada,
                    AeropuertoLlegada = a,
                    FechaSalida = v.FechaLlegada
                };
                vuelosVuelta = VuelosIda(vuelo, 1);
                if (vuelosVuelta != null)
                {
                    for (int i = 0; i < vuelosIda.Count; i++)
                    {
                        for (int y = 0; y < vuelosVuelta.Count; y++)
                        {
                            string text = "";
                            text += vuelosIda[i];
                            text += vuelosVuelta[y];

                            vuel.Add(text);
                        }
                    }

                }
            }
            return vuel;
        }

        /// <summary>
        /// Este metodo devuelve una lista de vuelos
        /// </summary>
        /// <param name="v">Representa los vuelos a buscar</param>
        /// <param name="tipo">Representa el formato a dar</param>
        /// <returns>Devuelve una lista de string con los vuelos</returns>
        public List<string> VuelosIda(Vuelo v, int tipo)
        {
            List<string> vuel = new List<string>();
            List<Vuelo> vuelos = new VueloDAO().CargarVuelosSimple(v);

            if (vuelos != null)
            {
                foreach (Vuelo i in vuelos)
                {
                    string text = "";

                    text += (FormatoSimple(i, tipo));
                    vuel.Add(text);
                }

            }

            List<Vuelo> vuelosPrimeraEscala = Escala(v);
            if (vuelosPrimeraEscala != null)
            {
                foreach (Vuelo i in vuelosPrimeraEscala)
                {
                    Vuelo vuelo = new Vuelo
                    {
                        AeropuertoLlegada = i.AeropuertoSalida,
                        AeropuertoSalida = v.AeropuertoSalida,
                        FechaSalida = v.FechaSalida
                    };
                    vuelos = new VueloDAO().CargarVuelosSimple(vuelo);
                    if (vuelos != null)
                    {
                        foreach (Vuelo y in vuelos)
                        {


                            string formato = String.Format("Salida\n \t{0}\n Escala\n\t{1}", FormatoSimple(y, tipo), FormatoSimple(i, tipo));
                            vuel.Add(formato);


                        }
                    }

                }
                foreach (Vuelo i in vuelosPrimeraEscala)
                {


                    Vuelo vuelo = new Vuelo
                    {
                        AeropuertoLlegada = i.AeropuertoSalida,
                        AeropuertoSalida = v.AeropuertoSalida,
                        FechaSalida = v.FechaSalida
                    };
                    List<Vuelo> vuelosSegundaEscala = new VueloDAO().Escala(vuelo);
                    if (vuelosSegundaEscala != null)
                    {

                        foreach (Vuelo y in vuelosSegundaEscala)
                        {
                            if (!y.AeropuertoSalida.Nombre.Equals(v.AeropuertoLlegada.Nombre) && !y.AeropuertoLlegada.Nombre.Equals(v.AeropuertoSalida.Nombre))
                            {
                                vuelo.AeropuertoLlegada = y.AeropuertoSalida;
                                vuelos = new VueloDAO().CargarVuelosSimple(vuelo);
                                if (vuelos != null)
                                {
                                    foreach (Vuelo z in vuelos)
                                    {

                                        string text = "";
                                        text += FormatoSimple(z, tipo);
                                        text += FormatoSimple(y, tipo);
                                        text += FormatoSimple(i, tipo);
                                        string formato = String.Format("Salida\n \t{0}\n Escala\n\t{1}\n Escala\n\t{2}", 
                                            FormatoSimple(z, tipo), FormatoSimple(y, tipo),FormatoSimple(i,tipo));
                                        vuel.Add(formato);


                                    }
                                }



                            }

                        }
                    }


                }

            }



            return vuel;
        }

        /// <summary>
        /// Este metodo devuelve una lista de escalas
        /// </summary>
        /// <param name="v">Representa los vuelos a buscar</param>
        /// <returns>Devuelve una lista de posibles escalas</returns>
        public List<Vuelo> Escala(Vuelo v)
        {
            List<Vuelo> vuelos = new VueloDAO().Escala(v);
            if (vuelos != null)
            {
                return vuelos;

            }
            return null;


        }





        /// <summary>
        /// Este metodo devuelve un string con formato de los vuelos
        /// </summary>
        /// <param name="v">Representa el vuelo</param>
        /// <param name="tipo">Representa el tipo de formato</param>
        /// <returns>Devuelve un string con el formato</returns>
        public string FormatoSimple(Vuelo v, int tipo)
        {
            if (tipo == 1)
            {
                string text = "{0}\n" + "{1}\n   {2}-------*--------{3}\n \t\t{4}\n ";
                return String.Format(text, v.Id, v.Aerolinea, v.AeropuertoSalida.Iata,
                    v.AeropuertoLlegada.Iata, v.HoraSalida, 5);
            }
            else if (tipo == 2)
            {
                string text = "{0}\n" + "{1}\n   {2}-------*--------{3}\n \t\t{4}\n Precio: {5}\n";
                return String.Format(text, v.Id, v.Aerolinea, v.AeropuertoSalida.Iata,
                    v.AeropuertoLlegada.Iata, v.HoraSalida, v.Precio);

            }
            else if (tipo == 3)
            {
                string text = "{0}\n" + "{1}\n   {2}-------*--------{3}\n \t\t{4}\n Duración: {5}\n";
                return String.Format(text, v.Id, v.Aerolinea, v.AeropuertoSalida.Iata,
                    v.AeropuertoLlegada.Iata, v.HoraSalida, v.Duracion);

            }


            return null;

        }


        /// <summary>
        /// Este metodo devuelve una lista de string
        /// </summary>
        /// <param name="v">Representa el vuelo a buscar</param>
        /// <param name="r">Representa las fechas a buscar</param>
        /// <param name="tipo">Representa el orden para mostrar</param>
        /// <returns>Devuelve una lista de string</returns>
        public List<string> VuelosInteligentes(Vuelo v, int r, int tipo)
        {
            List<string> vuelos = new List<string>();
            string txt = "";

            List<Vuelo> vuelosDirectos = new VueloDAO().CargarVuelosInteligentes(v, r, true);



            if (vuelosDirectos != null)
            {
                foreach (Vuelo i in vuelosDirectos)
                {

                    vuelos.Add(FormatoSimple(i, tipo));


                }

            }

            foreach (string item in VuelosInteligentesEcscala1(v, r, tipo))
            {
                if (item != null)
                {
                    vuelos.Add(item);

                }
            }


            foreach (string item in VuelosInteligentesEcscala2(v, r, tipo))
            {
                if (item != null)
                {
                    vuelos.Add(item);

                }
            }




            return vuelos;

        }


        /// <summary>
        /// Este metodo devuelve una lista de vuelos en formato de string
        /// </summary>
        /// <param name="v">Representa los vuelos a buscar</param>
        /// <param name="r">Representa el rango de fechas a buscar</param>
        /// <param name="tipo">Representa el orden de la lista</param>
        /// <returns>Devuelve una lista de string</returns>
        public List<string> VuelosInteligentesEcscala1(Vuelo v, int r, int tipo)
        {
            VueloDAO vdao = new VueloDAO();
            string txt = "";
            List<string> vuelos = new List<string>();
            List<Vuelo> vuelosIda = vdao.CargarVuelosInteligentesEscala(v, r, true);
            if (vuelosIda == null)
            {
                return null;
            }

            List<Vuelo> vuelosReg = vdao.CargarVuelosInteligentesEscala(v, r, false);

            if (vuelosReg == null)
            {
                return null;
            }




            foreach (Vuelo vi in vuelosIda)
            {
                foreach (Vuelo vr in vuelosReg)
                {
                    if (vi.FechaLlegada <= vr.FechaSalida)
                    {
                        if (vi.AeropuertoLlegada.Nombre == vr.AeropuertoSalida.Nombre)
                        {

                            txt += "Con escala \n ";
                            txt += FormatoSimple(vi, tipo);
                            txt += FormatoSimple(vr, tipo);
                            vuelos.Add(txt);
                            txt = "";


                        }




                    }

                }
            }


            return vuelos;

        }

        /// <summary>
        /// Este metodo devuelve una lista de vuelos en formato string
        /// </summary>
        /// <param name="v">Representa los vuelos a buscar</param>
        /// <param name="r">Representa el rango de fechas a buscar</param>
        /// <param name="tipo">Representa el orden de la lista</param>
        /// <returns>Devuelve una lista de vuelos en string</returns>
        public List<string> VuelosInteligentesEcscala2(Vuelo v, int r, int tipo)
        {
            VueloDAO vdao = new VueloDAO();
            string txt = "";
            List<string> vuelos = new List<string>();
            List<Vuelo> vuelosIda = vdao.CargarVuelosInteligentesEscala(v, r, true);
            if (vuelosIda == null)
            {
                return null;
            }
            List<Vuelo> vueloM = new VueloDAO().Seleccionar();
            if (vueloM == null)
            {
                return null;
            }




            List<Vuelo> vuelosReg = vdao.CargarVuelosInteligentesEscala(v, r, false);

            if (vuelosReg == null)
            {
                return null;
            }




            foreach (Vuelo vi in vuelosIda)
            {

                foreach (Vuelo vm in vueloM)
                {

                    foreach (Vuelo vr in vuelosReg)
                    {
                        if (vi.FechaLlegada <= vm.FechaSalida)
                        {
                            if (vi.AeropuertoLlegada.Nombre == vm.AeropuertoSalida.Nombre)
                            {
                                if (vm.FechaLlegada <= vr.FechaSalida)
                                {
                                    if (vm.AeropuertoLlegada.Nombre == vr.AeropuertoSalida.Nombre && vm.AeropuertoLlegada.Nombre != vi.AeropuertoSalida.Nombre)
                                    {

                                        txt += "Con doble escala \n ";
                                        txt += FormatoSimple(vi, tipo);
                                        txt += FormatoSimple(vm, tipo);

                                        txt += FormatoSimple(vr, tipo);
                                        vuelos.Add(txt);
                                        txt = "";



                                    }



                                }



                            }




                        }

                    }



                }
            }


            return vuelos;

        }

        /// <summary>
        /// Este metodo devuelve una lista de vuelos
        /// </summary>
        /// <param name="v">Representa el vuelo a buscar</param>
        /// <param name="r">Representa el rango de fechas a buscar</param>
        /// <returns>Devuelve una lista de vuelos en formato de string</returns>
        public List<string> VuelosVueltaInteligente(Vuelo v, int r)
        {
            VueloDAO vdao = new VueloDAO();

            List<string> vuelos = new List<string>();
            List<Vuelo> vuelosIda = vdao.CargarVuelosInteligentes(v, r, true);
            if (vuelosIda == null)
            {
                return null;
            }




            //Vuelo vuelta = new Vuelo()
            //{
            //    AeropuertoLlegada = v.AeropuertoSalida,
            //    AeropuertoSalida = v.AeropuertoLlegada,


            //};
            Aeropuerto a = v.AeropuertoSalida;
            v.AeropuertoSalida = v.AeropuertoLlegada;
            v.AeropuertoLlegada = a;

            List<Vuelo> vuelosReg = vdao.CargarVuelosInteligentes(v, r, false);

            if (vuelosReg == null)
            {
                return null;
            }
            for (int i = 0; i < vuelosIda.Count; i++)
            {
                string txt = "";
                for (int y = 0; y < vuelosReg.Count; y++)
                {



                    if (vuelosIda[i].FechaLlegada <= vuelosReg[y].FechaSalida)
                    {

                        txt += FormatoSimple(vuelosIda[i], 1);
                        txt += FormatoSimple(vuelosReg[y], 1);
                        vuelos.Add(txt);
                        txt = "";


                    }
                }

            }
            return vuelos;
        }


        /// <summary>
        /// Este metodo selecciona un vuelo por id
        /// </summary>
        /// <param name="id">Representa el id a buscar</param>
        /// <returns>Devuelve un vuelo</returns>
        public Vuelo SeleccionarID(int id)
        {

            return new VueloDAO().SeleccionarID(id);
        }




    }
}
