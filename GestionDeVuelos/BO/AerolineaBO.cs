﻿using GestionDeVuelos.DAO;
using GestionDeVuelos.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionDeVuelos.BO
{
    class AerolineaBO
    {
        /// <summary>
        /// Inserta una Aerolinea
        /// </summary>
        /// <param name="a">Es una Aerolinea </param>
        /// <returns>Inserta la Aerolinea en la base de datos</returns>
        public bool insertar(Aerolinea a)
        {
            return new AerolineaDAO().insertar(a);
        }

        /// <summary>
        /// Este metodo consulta una lista a la base de datos
        /// </summary>
        /// <returns> La lista de arolineas</returns>
        public List<Aerolinea> ConsultarDatos()
        { return new AerolineaDAO().ConsultarDatos();
        
        }


        }
}
