﻿using GestionDeVuelos.DAO;
using GestionDeVuelos.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionDeVuelos.BO
{
    class ReporteRankingVuelosBO
    {


        /// <summary>
        /// Carga una lista de aviones
        /// </summary>
        /// <param name="a"> aviones de acuerdo a una aerolinea</param>
        /// <returns>los aviones de una aerolinea</returns>
        public List<ReporteRankingVuelos> Seleccionar(Aerolinea a)
        {
            return new ReporteRankingVuelosDAO().Seleccionar(a);
        }
    }
}
