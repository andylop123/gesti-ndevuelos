﻿using GestionDeVuelos.DAO;
using GestionDeVuelos.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionDeVuelos.BO
{
    class AeropuertoBO
    {
        /// <summary>
        /// Inserta un Aeropuerto
        /// </summary>
        /// <param name="a"> Es un aeropuerto</param>
        /// <returns>Inserta un aeropuerto en la base de datos</returns>
        public bool Insertar(Aeropuerto a)
        {
            return new AeropuertoDAO().Insertar(a);
        }

        /// <summary>
        /// Carga una lista con un aeropuerto por un filto
        /// </summary>
        /// <param name="filtro"> Filtro por el cual se va cargar el aeropuerto</param>
        /// <returns>Devuelve la lista de aeropuertos</returns>
        public List<Aeropuerto> Seleccionar(string filtro)
        {
            return new AeropuertoDAO().Seleccionar(filtro);
        }


        /// <summary>
        /// Carga un aeropuerto por un filto
        /// </summary>
        /// <param name="filtro"> Filtro por el cual se va cargar el aeropuerto</param>
        /// <returns>Devuelve el aeropuerto</returns>
        public Aeropuerto CargarAeropuerto(string filtro)
        {
            return new AeropuertoDAO().CargarAeropuerto(filtro);
        }
    }
}
