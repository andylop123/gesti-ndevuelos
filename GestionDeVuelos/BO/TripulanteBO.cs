﻿using GestionDeVuelos.DAO;
using GestionDeVuelos.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionDeVuelos.BO
{
    class TripulanteBO
    {

        /// <summary>
        /// Inserta un Tripulante
        /// </summary>
        /// <param name="a"> Es un Tripulante</param>
        /// <returns>Inserta un Tripilante en la base de datos</returns>
        public bool insertar(Tripulacion t)
        {
            return new TripulacionDAO().insertar(t);
        }


        /// <summary>
        /// Este metodo consulta una lista a la base de datos
        /// </summary>
        /// <returns> La lista de Tripulacion</returns>
        public List<Tripulacion> ConsultarDatos()
        {
            return new TripulacionDAO().ConsultarDatos();
        }


        /// <summary>
        /// Devuelve una lista de tripulantes de acuerdo a un vuelo
        /// </summary>
        /// <param name="nombre">filtro para traer la lista</param>
        /// <returns> la lista de tripulantes</returns>
        public List<Tripulacion> Consulta4(string nombre)
        {
            return new TripulacionDAO().Consulta4(nombre);
        }


        /// <summary>
        /// Devuelve una lista de tripulantes de acuerdo a un vuelo
        /// </summary>
        /// <param name="v">vuelo para traer la lista</param>
        /// <returns> la lista de tripulantes</returns>
        public List<Tripulacion> Seleccionar(Vuelo v)
        {
            return new TripulacionDAO().Seleccionar(v);
        }





        /// <summary>
        /// Devuelve una lista de tripulantes de acuerdo a un id
        /// </summary>
        /// <param name="tripulacion">filtro para traer la lista</param>
        /// <returns> la lista de tripulantes</returns>
        public List<Tripulacion> CargarTripulacion(string tripulacion)
        {
            string[] ids = tripulacion.Split('-');
            List<Tripulacion> t = new List<Tripulacion>();
            foreach (string i in ids)
            {
                t.Add(new TripulacionDAO().CargarId(Int32.Parse(i)));
            }
            return t;
        }

    
    }
}
