﻿using GestionDeVuelos.DAO;
using GestionDeVuelos.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionDeVuelos.BO
{
    class UsuarioBO
    {
        /// <summary>
        /// Este metodo bucca un usuario en la base de datos
        /// </summary>
        /// <param name="usu">usuario a buscar</param>
        /// <returns>devuele el usuario</returns>
        public Usuario Login(Usuario usu) {
            usu.Contrasena= new UsuarioDAO().contraMD5(usu.Contrasena);
            return new UsuarioDAO().Login(usu);
        }


        /// <summary>
        /// Inserta un Usuario
        /// </summary>
        /// <param name="usu"> Es un Usuario</param>
        /// <returns>Inserta un usuario en la base de datos</returns>
        public bool Insertar(Usuario usu) {

            
            usu.Contrasena= new UsuarioDAO().contraMD5(usu.Contrasena);
            return new UsuarioDAO().Insertar(usu);
        }

       
    }
}
