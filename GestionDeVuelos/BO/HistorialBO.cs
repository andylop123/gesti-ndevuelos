﻿using GestionDeVuelos.DAO;
using GestionDeVuelos.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionDeVuelos.BO
{
    class HistorialBO
    {


        /// <summary>
        /// Inserta un vuelo comprado
        /// </summary>
        /// <param name="a"> Es un vuelo comprado</param>
        /// <returns>Inserta un vuelo o varios en la base de datos</returns>
        public bool Insertar(Historial h)
        {

            return new HistorialDAO().Insertar(h);
        }

       
    }
}
