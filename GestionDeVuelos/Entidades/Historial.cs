﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionDeVuelos.Entidades
{
    class Historial
    {

        public int Id { get; set; }
        public string cedula { get; set; }
        public string paisSalida { get; set; }
        public string paisLlegada { get; set; }

        public DateTime fechaHora { get; set; }

        public string paisEscala1 { get; set; }
        public string paisEscala2 { get; set; }
        public int costo { get; set; }

        public int duracion { get; set; }





    }
}
