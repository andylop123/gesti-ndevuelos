﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace GestionDeVuelos.Entidades
{
    public class Avion
    {
        public int Id { get; set; }
        public Aerolinea Aerolinea { get; set; }
        public string Modelo { get; set; }
        public string Ano { get; set;}
        public string Capacidad { get; set; }
        public string Estado { get; set; }

        public override string ToString()
        {
            return Id+"-"+Modelo;
        }

    }
}
