﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionDeVuelos.Entidades
{
    public class Vuelo
    {

        public int Id { get; set; }
        public Aerolinea Aerolinea { get; set; }
        public int Precio { get; set; }
        public DateTime FechaSalida { get; set; }
        public string HoraSalida { get; set; }
        public Aeropuerto AeropuertoSalida { get; set; }
        public DateTime FechaLlegada { get; set; }
        public string HoraLLegada { get; set; }
        public Aeropuerto AeropuertoLlegada { get; set; }
        public int Duracion { get; set; }
        public Avion Avion{ get; set; }
        public string Tripulacion { get; set; }


        public string Tipo { get; set; }
    }
}
