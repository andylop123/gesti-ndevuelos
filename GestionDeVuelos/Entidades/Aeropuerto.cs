﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionDeVuelos.Entidades
{
    public class Aeropuerto
    {
        public int Id { get; set; }
        public string Iata { get; set; }
        public string Nombre { get; set; }
        public string Pais { get; set; }

        public override string ToString()
        {
            return Nombre;
        }
    }
}
