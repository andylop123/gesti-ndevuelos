﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionDeVuelos.Entidades
{
    class Tripulacion
    {

        public int Id { get; set; }
        public string cedula { get; set; }
        public string nombre { get; set; }

        public DateTime nacimiento { get; set; }

        public Aerolinea aerolinea { get; set; }

        public string rol { get; set; }

        public string estado { get; set; }

        public override string ToString()
        {
            return String.Format(" {0} ",nombre);
        }
    }
}
